CREATE TABLE `LocationChangeLOG` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `location_level` tinyint(4) NOT NULL,
  `parent_old_id` int(11) NOT NULL,
  `parent_new_id` int(11) NOT NULL,
  `parent_level` tinyint(4) NOT NULL,
  `modules_updated` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci