CREATE TABLE `PackageItemsLOG` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `old_items` text NOT NULL,
  `new_items` text NOT NULL,
  `updated` datetime NOT NULL,
  `smaccount` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `package_id` (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8