CREATE TABLE `Location_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_2` int(11) NOT NULL DEFAULT '0',
  `location_1` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `abbreviation` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `friendly_url` (`friendly_url`),
  KEY `state_id` (`location_2`),
  KEY `name` (`name`),
  KEY `location_2_level` (`location_1`,`location_2`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci