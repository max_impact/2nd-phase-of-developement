CREATE TABLE `Forgot_Password` (
  `account_id` int(11) NOT NULL DEFAULT '0',
  `unique_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entered` date NOT NULL DEFAULT '0000-00-00',
  `section` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  KEY `account_id` (`account_id`),
  KEY `unique_key` (`unique_key`),
  KEY `entered` (`entered`),
  KEY `section` (`section`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci