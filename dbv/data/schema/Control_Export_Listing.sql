CREATE TABLE `Control_Export_Listing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) NOT NULL DEFAULT '0',
  `last_run_date` datetime NOT NULL,
  `total_listing_exported` int(11) NOT NULL,
  `last_listing_id` int(11) NOT NULL,
  `block` int(11) NOT NULL,
  `finished` varchar(1) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `type` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'csv / csv - data',
  `running_cron` varchar(1) NOT NULL,
  `scheduled` varchar(1) NOT NULL,
  PRIMARY KEY (`id`,`domain_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8