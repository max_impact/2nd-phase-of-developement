CREATE TABLE `Package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `parent_domain` int(11) DEFAULT NULL,
  `module` varchar(50) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `status` varchar(1) NOT NULL,
  `image_id` int(11) NOT NULL,
  `thumb_id` int(11) NOT NULL,
  `show_info` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `entered` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_domain` (`parent_domain`),
  KEY `module` (`module`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8