<?
    /*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /theme/default/frontend/claim.php
	# ----------------------------------------------------------------------------------------------------

    include(INCLUDES_DIR."/code/newsletter.php");

    $defaultusername = $username;
    $defaultpassword = "";
    if (DEMO_MODE) {
        $defaultusername = "demo@demodirectory.com";
        $defaultpassword = "abc123";
    }
?>

    <div class="real-steps">
        <div  class="standardStep-Title">
            <?=string_strtoupper(system_showText(LANG_EASYANDFAST));?> <span><?=string_strtoupper(system_showText(LANG_THREESTEPS))?></span>
        </div>
        <ul class="standardStep steps-3">
            <li class="steps-ui stepLast"><span>3</span>&nbsp;<?=system_showText(LANG_CHECKOUT)?></li>
            <li class="steps-ui"><span>2</span>&nbsp;<?=system_showText(LANG_LISTINGUPDATE)?></li>
            <li class="steps-ui stepActived"><span>1</span>&nbsp;<?=system_showText(LANG_ACCOUNTSIGNUP)?></li>
        </ul>
    </div>
		
    <div class="row-fluid">

        <div class="span12">
            <h4><?=system_showText(LANG_LISTING_CLAIMTHIS)?></h4>
        </div>

        <div class="extendedContent contentBorder">
            <?
            $listing = $listingObject;
            $levelObj = new ListingLevel();

            /**
            * This variable is used on view_listing_summary.php
            */
            if (TWILIO_APP_ENABLED == "on") {
                if (TWILIO_APP_ENABLED_SMS == "on") {
                    $levelsWithSendPhone = system_retrieveLevelsWithInfoEnabled("has_sms");
                } else {
                    $levelsWithSendPhone = false;
                }
                if (TWILIO_APP_ENABLED_CALL == "on") {
                    $levelsWithClicktoCall = system_retrieveLevelsWithInfoEnabled("has_call");
                } else {
                    $levelsWithClicktoCall = false;
                }
            } else {
                $levelsWithSendPhone = false;
                $levelsWithClicktoCall = false;
            }
            
            setting_get('commenting_edir', $commenting_edir);
            setting_get("review_listing_enabled", $review_enabled);
            $levelsWithReview = system_retrieveLevelsWithInfoEnabled("has_review");

            include(INCLUDES_DIR."/views/view_listing_summary.php");
            unset($listing, $levelObj);
            ?>
        </div>
        
        <div style="display:none">
            
            <form id="formDirectory" name="formDirectory" method="post" action="<?=((SSL_ENABLED == "on" && FORCE_MEMBERS_SSL == "on") ? SECURE_URL : NON_SECURE_URL);?>/<?=MEMBERS_ALIAS?>/login.php?destiny=<?=EDIRECTORY_FOLDER?>/<?=MEMBERS_ALIAS?>/claim/getlisting.php&amp;query=claimlistingid=<?=$claimlistingid?>">
                <input type="hidden" name="userform" value="directory" />
                <input type="hidden" name="claim" value="yes" />
                <input type="hidden" name="destiny" value="<?=$destiny?>" />
                <input type="hidden" name="query" value="<?=urlencode($query)?>" />
                
                <input type="hidden" name="username" id="form_username" value="" />
                <input type="hidden" name="password" id="form_password" value="" />
            </form>
            
            <? if ($openIDEnabled) { ?>
        
            <form id="formOpenID" name="formOpenID" method="post" action="<?=((SSL_ENABLED == "on" && FORCE_MEMBERS_SSL == "on") ? SECURE_URL : NON_SECURE_URL);?>/<?=MEMBERS_ALIAS?>/login.php?destiny=<?=EDIRECTORY_FOLDER?>/<?=MEMBERS_ALIAS?>/claim/getlisting.php&amp;query=claimlistingid=<?=$claimlistingid?>">

                <input type="hidden" name="userform" value="openid" />
                <input type="hidden" name="claim" value="yes" />
                <input type="hidden" name="destiny" value="<?=$destiny?>" />
                <input type="hidden" name="query" value="<?=urlencode($query)?>" />

                <input type="hidden" name="openidurl" id="form_openidurl" value="" />

            </form>

            <? } ?>
            
            <? if ($cUserEnabled) { ?>
        
                <form id="formCurrentUser" name="formCurrentUser" method="post" action="<?=((SSL_ENABLED == "on" && FORCE_MEMBERS_SSL == "on") ? SECURE_URL : NON_SECURE_URL);?>/<?=MEMBERS_ALIAS?>/login.php?destiny=<?=EDIRECTORY_FOLDER?>/<?=MEMBERS_ALIAS?>/claim/getlisting.php&amp;query=claimlistingid=<?=$claimlistingid?>">
                    <input type="hidden" name="userform" value="currentuser" />
                    <input type="hidden" name="claim" value="yes" />
                    <input type="hidden" name="acc" value="<?=sess_getAccountIdFromSession()?>" />

                    <?
                    if (sess_getAccountIdFromSession()) {
                        $msgLogged = system_showText(LANG_ADVERTISE_LOGGED_AS);
                        $dbObjWelcome = db_getDBObject(DEFAULT_DB, true);
                        $sqlWelcome = "SELECT C.first_name, C.last_name, A.has_profile, P.friendly_url, P.nickname FROM Contact C
                            LEFT JOIN Account A ON (C.account_id = A.id)
                            LEFT JOIN Profile P ON (P.account_id = A.id)
                            WHERE A.id = ".sess_getAccountIdFromSession();
                        $resultWelcome = $dbObjWelcome->query($sqlWelcome);
                        $contactWelcome = mysql_fetch_assoc($resultWelcome);

                        if ($contactWelcome["has_profile"] == "y") {
                            $msgLogged .= " ".$contactWelcome["nickname"].".";
                        } else {
                            $msgLogged .= " ". $contactWelcome["first_name"]." ".$contactWelcome["last_name"].".";
                        }

                        $msgLogged .= " <a href=\"".SOCIALNETWORK_URL."/logout.php?claim=true\">".system_showText(LANG_BUTTON_LOGOUT)."</a>";
                    }
                    ?>
                </form>

                <? } ?>
            
        </div>

        <form name="signup_claim" action="<?=system_getFormAction($_SERVER["REQUEST_URI"])?>" method="post" class="standardForm">

            <input type="hidden" name="claim" value="true" />
            <input type="hidden" name="claimlistingid" id="claimlistingid" value="<?=$claimlistingid?>" />

            <div class="order">

                <div id="identification">

                    <div class="left textright">
                        <h3><?=system_showText(LANG_ALREADYHAVEACCOUNT);?></h3>
                        <p><?=system_showText(LANG_ADVERTISE_SIGNUP);?></p>
                    </div>

                    <div class="right">

                        <div class="content-custom cont_100 textcenter">
                            <h3 class="cont_50"><?=system_showText(LANG_ADVERTISE_SIGNUP_ALREADYUSER);?></h3>
                            <h3 class="cont_50"><?=system_showText(LANG_ADVERTISE_SIGNUP_NEWUSER);?></h3>
                        </div>

                        <div class="clear">&nbsp;</div>

                        <div class="cont_100 identification">

                            <div class="signup">
                                <label><?=system_showText(LANG_ACCOUNTDIRECTORYUSER);?></label>

                                <div class="inputimg large">
                                    <i class="inpemail"></i><input type="email" name="dir_username" id="dir_username" class="" placeholder="<?=system_showText(LANG_LABEL_USERNAME);?>" value="<?=$defaultusername?>" />
                                </div>

                                <div class="inputimg large">
                                    <i class="inppassword"></i><input type="password"  name="dir_password" id="dir_password" class="" placeholder="<?=system_showText(LANG_LABEL_PASSWORD);?>" value="<?=$defaultpassword?>" />
                                </div>

                                <p class="standardButton buttoncenter">
                                    <button type="button" onclick="submitForm('formDirectory');"><?=system_showText(LANG_BUTTON_LOGIN);?></button>
                                </p>

                                <? if ($facebookEnabled || $googleEnabled) { ?>

                                <label><?=system_showText(LANG_ACCOUNTFBGOOGLEUSER);?></label>

                                <p>
                                    <? 
                                    if ($facebookEnabled) {
                                        $summaryForm = true;
                                        $urlRedirect = "?claim=yes&destiny=".urlencode(DEFAULT_URL."/".MEMBERS_ALIAS."/claim/getlisting.php?claimlistingid=".$claimlistingid);
                                        include(INCLUDES_DIR."/forms/form_facebooklogin.php");
                                    }

                                    if ($googleEnabled) {
                                        $summaryForm = true;
                                        $urlRedirect = "&claim=yes&destiny=".urlencode(DEFAULT_URL."/".MEMBERS_ALIAS."/claim/getlisting.php?claimlistingid=".$claimlistingid);
                                        include(INCLUDES_DIR."/forms/form_googlelogin.php");
                                    }
                                    ?>
                                </p>

                                <? } ?>

                                <? if ($openIDEnabled) { ?>

                                <label><?=system_showText(LANG_ACCOUNTOPENIDUSER);?></label>

                                <div class="standardButton btn-openid">
                                    <input type="text" class="openid" name="openidurl" id="openidurl" placeholder="<?=system_showText(LANG_LABEL_PROFILE_YOUROPENID);?>" value="<?=$openidurl?>" />
                                    <button type="button" onclick="submitForm('formOpenID');"><?=system_showText(LANG_BUTTON_LOGIN);?></button>
                                </div>

                                <? } ?>
                                
                            </div>

                            <div class="create">

                                <?
                                if ($message_account || $message_contact) {
                                    $msgError .= $message_contact;
                                    $msgError .= (string_strlen($msgError) ? "<br />" : "").$message_account;

                                    echo "<p class=\"errorMessage\">$msgError</p>";
                                }
                                ?>

                                <div>
                                    <label><?=system_showText(LANG_ADVERTISE_CREATE_ACC);?></label>

                                    <div>
                                        <div class="inputimg">
                                            <i class="inpname"></i><input type="text" name="first_name" placeholder="<?=system_showText(LANG_LABEL_FIRST_NAME);?>" value="<?=$first_name?>" />
                                        </div>
                                        <div class="inputimg">
                                            <i class="inpname"></i><input type="text" name="last_name" placeholder="<?=system_showText(LANG_LABEL_LAST_NAME);?>" value="<?=$last_name?>" />
                                        </div>
                                    </div>

                                    <div>
                                        <div class="inputimg large">
                                            <i class="inpemail"></i><input type="email" name="username" placeholder="<?=system_showText(LANG_LABEL_USERNAME)?>" value="<?=$username?>" maxlength="<?=USERNAME_MAX_LEN?>" onblur="checkUsername(this.value, '<?=DEFAULT_URL;?>', 'members', 0); populateField(this.value, 'email');"/>
                                        </div>
                                        <input type="hidden" name="email" id="email" value="<?=$email?>" />
                                        <label id="checkUsername">&nbsp;</label>
                                    </div>

                                    <div class="cont_50">
                                        <div class="inputimg">
                                            <i class="inppassword"></i><input type="password" placeholder="<?=system_showText(LANG_LABEL_CREATE_PASSWORD)?>" name="password" maxlength="<?=PASSWORD_MAX_LEN?>" />
                                        </div>
                                        <div class="inputimg">
                                            <i class="inppassword"></i><input type="password" placeholder="<?=system_showText(LANG_LABEL_RETYPE_PASSWORD);?>" name="retype_password" />
                                        </div>
                                    </div>

                                    <? if ($showNewsletter) { ?>
                                    <div class="option">
                                        <input type="checkbox" id="inewsletter" class="checkbox" name="newsletter" value="y" <?=($newsletter || (!$newsletter && $_SERVER["REQUEST_METHOD"] != "POST")) ? "checked" : ""?> />
                                        <label for="inewsletter"><?=$signupLabel?></label>
                                    </div>
                                    <? } ?>

                                    <div class="option">
                                        <input id="checkterms" type="checkbox" class="checkbox" name="agree_tou" value="1" <?=($agree_tou) ? "checked" : ""?> />
                                        <label for="checkterms"><a rel="nofollow" href="<?=DEFAULT_URL?>/popup/popup.php?pop_type=terms" class="fancy_window_iframe"><?=system_showText(LANG_IGREETERMS)?></a></label>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="blockcontinue cont_100">

                    <div id="check_out_free_2">

                        <div class="cont_60 ">&nbsp;</div>

                        <div class="cont_40 ">
                            <p class="checkoutButton bt-highlight">
                                <? if ($msgLogged) { ?>
                                
                                <button type="button" onclick="submitForm('formCurrentUser');"><?=system_showText(LANG_BUTTON_NEXT)?></button>
                                <em><?=$msgLogged;?></em>
                                
                                <? } else { ?>
                                
                                <button type="submit" name="next" value="<?=system_showText(LANG_LISTING_CLAIMTHIS)?>"><?=system_showText(LANG_LISTING_CLAIMTHIS)?></button>
                                
                                <? } ?>
                                
                            </p>
                        </div>

                    </div>

                </div>

            </div>

        </form>

    </div>