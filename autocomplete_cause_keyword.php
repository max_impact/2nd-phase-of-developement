<?

    /*==================================================================*\
    ######################################################################
    #                                                                    #
    # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
    #                                                                    #
    # This file may not be redistributed in whole or part.               #
    # eDirectory is licensed on a per-domain basis.                      #
    #                                                                    #
    # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
    #                                                                    #
    # http://www.edirectory.com | http://www.edirectory.com/license.html #
    ######################################################################
    \*==================================================================*/

    # ----------------------------------------------------------------------------------------------------
    # * FILE: /autocomplete_keyword.php
    # ----------------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------------
    # LOAD CONFIG
    # ----------------------------------------------------------------------------------------------------
    include("./conf/loadconfig.inc.php");

    header("Content-Type: text/html; charset=".EDIR_CHARSET, TRUE);

    # ----------------------------------------------------------------------------------------------------
    # LANGUAGE VERIFICATION
    # ----------------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------------
    # INPUT VERIFICATION
    # ----------------------------------------------------------------------------------------------------
    $limit = $_GET['limit'] ? db_formatNumber($_GET['limit']) : AUTOCOMPLETE_MAXITENS;
    $module   = isset($_GET['module']) ? $_GET['module'] : false;
    $input    = string_strtolower(trim($_GET["q"]));
    $whereStr = db_formatString('%'.$input.'%');
    /*
     * Keyword for title of Listing
     */
    $whereStr_Listing = db_formatString($input."*");
   

    # ----------------------------------------------------------------------------------------------------
    # SUPPORT FUNCTIONS
    # ----------------------------------------------------------------------------------------------------
    
    function getSQLCategorieSearch($moduleName) {

        global $whereStr, $limit;

        $tableCategory = ucfirst($moduleName).'Category';
       
        $whereLike   = array();
        //adding title search
        $whereLike[] = " title LIKE $whereStr ";
        //adding keywords search
        $whereLike[] = " keywords LIKE $whereStr ";
        //adding seo_keywords search
        $whereLike[] = " seo_keywords LIKE $whereStr ";
        //creating the where condition
        $whereLike = count($whereLike) ? implode(' OR ', $whereLike) : '';
        //creating the sql
        $sql = "SELECT title, (".db_formatString(@constant('LANG_'.string_strtoupper($moduleName).'_FEATURE_NAME_PLURAL')).") AS module FROM $tableCategory WHERE 1 AND (".$whereLike.") AND enabled = 'y' ORDER BY title LIMIT $limit";
    
        return $sql;
        
    }
    
   

	
    # ----------------------------------------------------------------------------------------------------
    # AUTO COMPLETE
    # ----------------------------------------------------------------------------------------------------
    if($input){
        
        $rows = array();
        $dbObj_main = db_getDBObject(DEFAULT_DB,true);
        $dbObj = db_getDBObjectByDomainID(0,$dbObj_main,$_SERVER["HTTP_HOST"]);
        
        //listing
        if ('listing' == $module || !$module ) {
            $sql   = getSQLCategorieSearch('listing');
            //$_rows = $dbObj->query($sql);
            $_rows = $dbObj->unbuffered_query($sql);
            while ($row = mysql_fetch_array($_rows)){
                if ($row['title']){
                    $rows[] = $row;
                }
            }
            
            //titles
            $sql   = getSQLTitleSearch('listing');
            //$_rows = $dbObj->query($sql);
            $_rows = $dbObj->unbuffered_query($sql);
            while ($row = mysql_fetch_array($_rows)){
                if ($row['title']){
                    $rows[] = $row;
                }
            }
        }
         if ('nonProfitListing' == $module || !$module ) {
            $sql   = getSQLCategorieSearch('nonProfitListing');
             //$_rows = $dbObj->query($sql);
            $_rows = $dbObj->unbuffered_query($sql);
            while ($row = mysql_fetch_array($_rows)){
                if ($row['title']){
                    $rows[] = $row;
                }
            }
            
            //titles
//            $sql   = getSQLTitleSearch('nonProfitListing');
//            //$_rows = $dbObj->query($sql);
//            $_rows = $dbObj->unbuffered_query($sql);
//            while ($row = mysql_fetch_array($_rows)){
//                if ($row['title']){
//                    $rows[] = $row;
//                }
//            }
        }
       
        $aResults = array();
        foreach ($rows as $row) {
            if (!in_array($row['title'], $aResults)) {
                    $aResults[] = ($row["title"].'|'.$row["id"]);
            }
        }
        
        echo implode("\n", $aResults);		
		
	}