<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /conf/default_themecolors.inc.php
	# ----------------------------------------------------------------------------------------------------

	//Theme: Default
	//Scheme Color: Default
	$arrayColors["default"]["default"]["colorBackground"] = "F1F1F1";
	$arrayColors["default"]["default"]["colorContentBackground"] = false; //none
	$arrayColors["default"]["default"]["colorMainContent"] = "FFFFFF";
	$arrayColors["default"]["default"]["colorSlider"] = "E7E7E7";
	$arrayColors["default"]["default"]["colorTitle"] = "5C6B77";
	$arrayColors["default"]["default"]["colorTitleBorder"] = "E6E8E9";
	$arrayColors["default"]["default"]["colorText"] = "5C6B77";
	$arrayColors["default"]["default"]["colorLink"] = "004276";
	$arrayColors["default"]["default"]["colorUserNavbar"] = "83939F";
	$arrayColors["default"]["default"]["colorUserNavbarText"] = "DEE4E9";
	$arrayColors["default"]["default"]["colorUserNavbarLink"] = "2D3F4D";
	$arrayColors["default"]["default"]["colorNavbar"] = false; //image
	$arrayColors["default"]["default"]["colorNavbarLink"] = "FFFFFF";
	$arrayColors["default"]["default"]["colorNavbarLinkActive"] = "597F9B";
	$arrayColors["default"]["default"]["colorFooter"] = false; //image
	$arrayColors["default"]["default"]["colorFooterText"] = "62707B";
	$arrayColors["default"]["default"]["colorFooterLink"] = "597F9B";
	$arrayColors["default"]["default"]["fontOption"] = "1"; //Arial,Helvetica,sans-serif
	
	//Theme: Default
	//Scheme Color: Custom
	$arrayColors["default"]["custom"]["colorBackground"] = false; 
	$arrayColors["default"]["custom"]["colorContentBackground"] = false;
	$arrayColors["default"]["custom"]["colorMainContent"] = false;
	$arrayColors["default"]["custom"]["colorSlider"] = false;
	$arrayColors["default"]["custom"]["colorTitle"] = false;
	$arrayColors["default"]["custom"]["colorTitleBorder"] = false;
	$arrayColors["default"]["custom"]["colorText"] = false;
	$arrayColors["default"]["custom"]["colorLink"] = false;
	$arrayColors["default"]["custom"]["colorUserNavbar"] = false;
	$arrayColors["default"]["custom"]["colorUserNavbarText"] = false;
	$arrayColors["default"]["custom"]["colorUserNavbarLink"] = false;
	$arrayColors["default"]["custom"]["colorNavbar"] = false; //image
	$arrayColors["default"]["custom"]["colorNavbarLink"] = false;
	$arrayColors["default"]["custom"]["colorNavbarLinkActive"] = false;
	$arrayColors["default"]["custom"]["colorFooter"] = false; //image
	$arrayColors["default"]["custom"]["colorFooterText"] = false;
	$arrayColors["default"]["custom"]["colorFooterLink"] = false;
	$arrayColors["default"]["custom"]["fontOption"] = "1"; //Arial,Helvetica,sans-serif
    
    //Theme: Real Estate
	//Scheme Color: Real Estate
	$arrayColors["realestate"]["realestate"]["colorBackground"] = "F1F1F1";
	$arrayColors["realestate"]["realestate"]["colorContentBackground"] = false; //none
	$arrayColors["realestate"]["realestate"]["colorMainContent"] = "FFFFFF";
	$arrayColors["realestate"]["realestate"]["colorSlider"] = "E7E7E7";
	$arrayColors["realestate"]["realestate"]["colorTitle"] = "5C6B77";
	$arrayColors["realestate"]["realestate"]["colorTitleBorder"] = "E6E8E9";
	$arrayColors["realestate"]["realestate"]["colorText"] = "5C6B77";
	$arrayColors["realestate"]["realestate"]["colorLink"] = "004276";
	$arrayColors["realestate"]["realestate"]["colorUserNavbar"] = "83939F";
	$arrayColors["realestate"]["realestate"]["colorUserNavbarText"] = "DEE4E9";
	$arrayColors["realestate"]["realestate"]["colorUserNavbarLink"] = "2D3F4D";
	$arrayColors["realestate"]["realestate"]["colorNavbar"] = false; //image
	$arrayColors["realestate"]["realestate"]["colorNavbarLink"] = "FFFFFF";
	$arrayColors["realestate"]["realestate"]["colorNavbarLinkActive"] = "597F9B";
	$arrayColors["realestate"]["realestate"]["colorFooter"] = false; //image
	$arrayColors["realestate"]["realestate"]["colorFooterText"] = "62707B";
	$arrayColors["realestate"]["realestate"]["colorFooterLink"] = "597F9B";
	$arrayColors["realestate"]["realestate"]["fontOption"] = "1"; //Arial,Helvetica,sans-serif
    
    //Theme: Real Estate
	//Scheme Color: Custom
	$arrayColors["realestate"]["custom"]["colorBackground"] = false; 
	$arrayColors["realestate"]["custom"]["colorContentBackground"] = false;
	$arrayColors["realestate"]["custom"]["colorMainContent"] = false;
	$arrayColors["realestate"]["custom"]["colorSlider"] = false;
	$arrayColors["realestate"]["custom"]["colorTitle"] = false;
	$arrayColors["realestate"]["custom"]["colorTitleBorder"] = false;
	$arrayColors["realestate"]["custom"]["colorText"] = false;
	$arrayColors["realestate"]["custom"]["colorLink"] = false;
	$arrayColors["realestate"]["custom"]["colorUserNavbar"] = false;
	$arrayColors["realestate"]["custom"]["colorUserNavbarText"] = false;
	$arrayColors["realestate"]["custom"]["colorUserNavbarLink"] = false;
	$arrayColors["realestate"]["custom"]["colorNavbar"] = false; //image
	$arrayColors["realestate"]["custom"]["colorNavbarLink"] = false;
	$arrayColors["realestate"]["custom"]["colorNavbarLinkActive"] = false;
	$arrayColors["realestate"]["custom"]["colorFooter"] = false; //image
	$arrayColors["realestate"]["custom"]["colorFooterText"] = false;
	$arrayColors["realestate"]["custom"]["colorFooterLink"] = false;
	$arrayColors["realestate"]["custom"]["fontOption"] = "1"; //Arial,Helvetica,sans-serif
    
    //Theme: Dining Guide
	//Scheme Color: Dining Guide
	$arrayColors["diningguide"]["diningguide"]["colorBackground"] = "F1F1F1";
	$arrayColors["diningguide"]["diningguide"]["colorContentBackground"] = false; //none
	$arrayColors["diningguide"]["diningguide"]["colorMainContent"] = "FFFFFF";
	$arrayColors["diningguide"]["diningguide"]["colorSlider"] = "E7E7E7";
	$arrayColors["diningguide"]["diningguide"]["colorTitle"] = "5C6B77";
	$arrayColors["diningguide"]["diningguide"]["colorTitleBorder"] = "E6E8E9";
	$arrayColors["diningguide"]["diningguide"]["colorText"] = "5C6B77";
	$arrayColors["diningguide"]["diningguide"]["colorLink"] = "004276";
	$arrayColors["diningguide"]["diningguide"]["colorUserNavbar"] = "83939F";
	$arrayColors["diningguide"]["diningguide"]["colorUserNavbarText"] = "DEE4E9";
	$arrayColors["diningguide"]["diningguide"]["colorUserNavbarLink"] = "2D3F4D";
	$arrayColors["diningguide"]["diningguide"]["colorNavbar"] = false; //image
	$arrayColors["diningguide"]["diningguide"]["colorNavbarLink"] = "FFFFFF";
	$arrayColors["diningguide"]["diningguide"]["colorNavbarLinkActive"] = "597F9B";
	$arrayColors["diningguide"]["diningguide"]["colorFooter"] = false; //image
	$arrayColors["diningguide"]["diningguide"]["colorFooterText"] = "62707B";
	$arrayColors["diningguide"]["diningguide"]["colorFooterLink"] = "597F9B";
	$arrayColors["diningguide"]["diningguide"]["fontOption"] = "1"; //Arial,Helvetica,sans-serif
    
    //Theme: Dining Guide
	//Scheme Color: Custom
	$arrayColors["diningguide"]["custom"]["colorBackground"] = false; 
	$arrayColors["diningguide"]["custom"]["colorContentBackground"] = false;
	$arrayColors["diningguide"]["custom"]["colorMainContent"] = false;
	$arrayColors["diningguide"]["custom"]["colorSlider"] = false;
	$arrayColors["diningguide"]["custom"]["colorTitle"] = false;
	$arrayColors["diningguide"]["custom"]["colorTitleBorder"] = false;
	$arrayColors["diningguide"]["custom"]["colorText"] = false;
	$arrayColors["diningguide"]["custom"]["colorLink"] = false;
	$arrayColors["diningguide"]["custom"]["colorUserNavbar"] = false;
	$arrayColors["diningguide"]["custom"]["colorUserNavbarText"] = false;
	$arrayColors["diningguide"]["custom"]["colorUserNavbarLink"] = false;
	$arrayColors["diningguide"]["custom"]["colorNavbar"] = false; //image
	$arrayColors["diningguide"]["custom"]["colorNavbarLink"] = false;
	$arrayColors["diningguide"]["custom"]["colorNavbarLinkActive"] = false;
	$arrayColors["diningguide"]["custom"]["colorFooter"] = false; //image
	$arrayColors["diningguide"]["custom"]["colorFooterText"] = false;
	$arrayColors["diningguide"]["custom"]["colorFooterLink"] = false;
	$arrayColors["diningguide"]["custom"]["fontOption"] = "1"; //Arial,Helvetica,sans-serif
	
	define("ARRAY_DEFAULT_COLORS", serialize($arrayColors));
?>