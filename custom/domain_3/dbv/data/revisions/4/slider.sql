ALTER TABLE `Slider` ADD `slider_kind` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `target` ,
ADD `slider_loc1` INT( 11 ) NULL DEFAULT NULL AFTER `slider_kind` ,
ADD `slider_loc3` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `slider_loc1` ,
ADD `slider_loc4` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `slider_loc3` ,
ADD `slider_cat` INT( 11 ) NULL DEFAULT NULL AFTER `target` 
