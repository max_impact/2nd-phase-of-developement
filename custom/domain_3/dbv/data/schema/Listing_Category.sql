CREATE TABLE `Listing_Category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `listing_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `category_root_id` int(11) NOT NULL,
  `category_node_left` int(11) NOT NULL,
  `category_node_right` int(11) NOT NULL,
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `listing_id` (`listing_id`),
  KEY `category_id` (`category_id`),
  KEY `status` (`status`),
  KEY `category_status` (`category_id`,`status`),
  KEY `category_listing_id` (`category_id`,`category_root_id`,`listing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci