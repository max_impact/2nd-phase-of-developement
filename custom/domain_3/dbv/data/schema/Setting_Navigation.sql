CREATE TABLE `Setting_Navigation` (
  `order` int(11) NOT NULL,
  `label` varchar(100) CHARACTER SET utf8 NOT NULL,
  `link` varchar(255) CHARACTER SET utf8 NOT NULL,
  `area` varchar(20) CHARACTER SET utf8 NOT NULL,
  `custom` char(1) CHARACTER SET utf8 NOT NULL,
  `theme` varchar(30) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`order`,`area`,`theme`),
  KEY `label` (`label`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci