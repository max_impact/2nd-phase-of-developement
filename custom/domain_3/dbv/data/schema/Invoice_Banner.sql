CREATE TABLE `Invoice_Banner` (
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `banner_id` int(11) NOT NULL DEFAULT '0',
  `banner_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date DEFAULT NULL,
  `impressions` mediumint(9) NOT NULL DEFAULT '0',
  `amount` decimal(10,2) DEFAULT NULL,
  KEY `invoice_id` (`invoice_id`),
  KEY `banner_id` (`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci