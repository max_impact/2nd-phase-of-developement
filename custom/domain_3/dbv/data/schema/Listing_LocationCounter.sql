CREATE TABLE `Listing_LocationCounter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_level` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `full_friendly_url` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci