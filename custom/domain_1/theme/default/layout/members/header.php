<?
	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /theme/default/layout/members/header.php
	# ----------------------------------------------------------------------------------------------------

?>  
    <!--[if IE 7]><body class="ie ie7"><![endif]-->
    <!--[if lt IE 9]><body class="ie"><![endif]-->
    <!-- [if false]><body><![endif]-->
   
   
    <script type="text/javascript">
        $(document).ready(function() {

            //Toggle items on modules manage table
            var toolbaritemlast = $(".toolbar-icons > :last-child");
            $(toolbaritemlast).css({'border-right':'none',  'margin-right':' -3px'});

            $("div.toolbar-icons-button").click(function() {
                var position = $(this).position();
                var widthoolbar = $(this).children(".toolbar-icons").width();
                $(this).children(".toolbar-icons, .toolbararrow").toggle();
                $(this).children(".toolbar-icons").css({'left': position.left-widthoolbar/2, 'top': position.top-37});
            });
            
            $(".dropdown-toggle").click(function() {
                $(".dropdown-menu").toggle();
            });
 
            $("#main_menu").click(function() {
                $(".nav-collapse-members").toggleClass("in");
            });
        });
    </script>
    
    <? if (is_ie(true) || ($checkIE && $ieVersion == 7)) { ?>
        <div class="browserMessage">
            <div class="wrapper">
                <?=system_showText(LANG_IE6_WARNING);?>
            </div>
        </div>
    <? }
		
    /** Float Layer *******************************************************************/
    include(INCLUDES_DIR."/views/view_float_layer.php");
    /**********************************************************************************/
    
    system_increaseVisit(db_formatString(getenv("REMOTE_ADDR")));
   
    ?>
    
    <div class="navbar navbar-static-top">
            
        <div class="header-brand container">
            <!-- The function "system_getHeaderLogo()" returns a inline style, like style="background-image: url(YOUR LOGO URL HERE)" -->
            <div class="brand-logo">
            <a class="brand logo" id="logo-link" href="<?=NON_SECURE_URL?>/" target="_parent" <?=(trim(EDIRECTORY_TITLE) ? "title=\"".EDIRECTORY_TITLE."\"" : "")?> <?=system_getHeaderLogo();?>>
                <?=(trim(EDIRECTORY_TITLE) ? EDIRECTORY_TITLE : "&nbsp;")?>
            </a>
            </div>
        </div>
        
        <div class="navbar-inner">
                
            <div class="container">
                    
                <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                <a class="btn btn-navbar" id="main_menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <? if (sess_getAccountIdFromSession()) { ?>
                <div class="nav-collapse-members">
                    <ul class="nav" id="ul_main_menu">                            
                        <?
                        $accObj = new Account(sess_getAccountIdFromSession());
                        if ((string_strpos($_SERVER["PHP_SELF"], "".MEMBERS_ALIAS."/signup") === false) && (string_strpos($_SERVER["PHP_SELF"], "".MEMBERS_ALIAS."/claim") === false) && $accObj->getString("is_sponsor") == "y") {
                        ?>

                        <li <?=((string_strpos($_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"], $_SERVER["SERVER_NAME"].EDIRECTORY_FOLDER."/".MEMBERS_ALIAS."/index.php") !== false) ? "class=\"menuActived\"" : "")?>><a href="<?=DEFAULT_URL?>/<?=MEMBERS_ALIAS?>/"><?=system_showText(LANG_BUTTON_HOME)?></a></li>

                        <li><a href="<?=NON_SECURE_URL?>/"><?=system_showText(LANG_LABEL_BACK_TO_SEARCH);?></a></li>

                        <li <?=((string_strpos($_SERVER["PHP_SELF"], "/".MEMBERS_ALIAS."/account/account.php") !== false) ? "class=\"menuActived\"" : "")?>><a href="<?=DEFAULT_URL?>/<?=MEMBERS_ALIAS?>/account/account.php?id=<?=sess_getAccountIdFromSession()?>"><?=system_showText(LANG_BUTTON_MANAGE_ACCOUNT)?></a></li>

                       <? } ?>
                    </ul>
                    
                    <ul class="nav pull-right">
                        <? if (!empty($_SESSION[SM_LOGGEDIN])) { ?>
                        
                            <script language="javascript" type="text/javascript">
                                function sitemgrSection() {
                                    location = "<?=DEFAULT_URL?>/<?=MEMBERS_ALIAS?>/sitemgraccess.php?logout";
                                }
                            </script>
                            
                            <li><a href="javascript:sitemgrSection();"><?=system_showText(LANG_LABEL_SITEMGR_SECTION);?></a></li>
                            
                        <? } else { ?>

                            <li class="dropdown">
                                <a href="javascript: void(0);" class="sign-up dropdown-toggle">
                                    <i class="i-profile"></i><b class="caret"></b>
                                </a>
                                
                                <ul class="dropdown-menu">
                                    <li class="nav-header">
                                        <?=system_showText(LANG_LABEL_WELCOME)." ".($contactWelcome["has_profile"] == "y" ? (trim($contactWelcome["nickname"]) ? $contactWelcome["nickname"] : $contactWelcome["first_name"]." ".$contactWelcome["last_name"]) : $contactWelcome["first_name"]." ".$contactWelcome["last_name"]);?>!
                                    </li>
                                    
                                    <li class="divider"></li>
                                    
                                    <li>
                                        <a href="<?=((SSL_ENABLED == "on" && FORCE_MEMBERS_SSL == "on") ? SECURE_URL : NON_SECURE_URL)?>/<?=MEMBERS_ALIAS?>/"><?=system_showText(LANG_MEMBERS_DASHBOARD);?></a>
                                    </li>

                                    <li>
                                        <a href="<?=DEFAULT_URL."/".MEMBERS_ALIAS."/account/quicklists.php"?>"><?=system_showText(LANG_QUICK_LIST)?></a>
                                    </li>
                                    
                                    <li>
                                        <a href="<?=((SSL_ENABLED == "on" && FORCE_MEMBERS_SSL == "on") ? SECURE_URL : NON_SECURE_URL)?>/<?=MEMBERS_ALIAS?>/logout.php"><?=system_showText(LANG_BUTTON_LOGOUT)?></a>
                                    </li>
                                </ul>
                                
                            </li>
                        <? } ?>
                    </ul>
                </div>
                <? } ?>

            </div>
                
        </div>
            
    </div>
    
    <div class="image-bg">
        <?php //echo front_getBackground($customimage);?>
    </div>
    

    <div class="well container members">

        <div class="container-fluid">

                <? if (string_strpos($_SERVER["PHP_SELF"], "claim") === false && string_strpos($_SERVER["PHP_SELF"], "resetpassword") === false && string_strpos($_SERVER["PHP_SELF"], "forgot") === false && string_strpos($_SERVER["PHP_SELF"], "facebook") === false) { ?>
                    <p class="breadcrumb">
                        <?
                        $aux_breadcrumb = domain_BreadCrumb();
                        echo $aux_breadcrumb;
                        ?>
                    </p>
                <? } ?>