<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /theme/default/frontend/event_calendar.php
	# ----------------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------------
	# VALIDATE FEATURE
	# ----------------------------------------------------------------------------------------------------
	if (EVENT_FEATURE == "on" && CUSTOM_EVENT_FEATURE == "on") {

        $firstEvent = true;
        $showYear = true;
        calendar_getEventsDay($calendar, $showYear);

        if (count($calendar)) { ?>

            <div class="flex-box-group color-4">

                <h2>
                    <?=system_showText(LANG_UPCOMING_EVENT);?>

                    <? if ((
                            (ACTUAL_MODULE_FOLDER == "" || !defined("ACTUAL_MODULE_FOLDER")) || 
                            (ACTUAL_MODULE_FOLDER == EVENT_FEATURE_FOLDER && EVENT_SCALABILITY_OPTIMIZATION != "on")
                            )
                             && string_strpos($_SERVER["REQUEST_URI"], "results.php") === false
                            ) { ?>
                    
                    <a class="view-more" href="<?=(ACTUAL_MODULE_FOLDER == EVENT_FEATURE_FOLDER ? EVENT_DEFAULT_URL."/results.php" : EVENT_DEFAULT_URL."/");?>"><?=system_showText(LANG_LABEL_SEE_ALL);?></a>
                    
                    <? } ?>
                </h2>

                <ul class="calendar-event">

                    <? foreach ($calendar as $monthDay) {

                        if ($firstEvent) {
                            $triggerJS = "getEventsCalendar('{$monthDay["day"]}', '{$monthDay["month"]}', '{$monthDay["year"]}')";
                            $firstEvent = false;
                        }

                        ?>

                    <li id="<?=$monthDay["day"].$monthDay["month"].$monthDay["year"]?>" onclick="getEventsCalendar('<?=$monthDay["day"]?>', '<?=$monthDay["month"]?>', '<?=$monthDay["year"]?>');">
                        <b><?=(EDIR_LANGUAGE == "en_us" ? system_getOrdinalLabel($monthDay["day"]) : $monthDay["day"])?></b><br />
                        <span><?=$monthDay["month_label"].($showYear ? "<br />".$monthDay["year"] : "")?></span>
                    </li>

                    <? } ?>

                </ul>

                <p class="calendar_loading" style="display:none"></p>

                <div id="calendar_event"></div>
                
                <input type="hidden" id="last_day_click" value="" />

            </div>

            <? if ($triggerJS) { ?>

            <script language="javascript" type="text/javascript">
                $(document).ready(function(){
                    <?=$triggerJS;?>
                });
            </script>

            <? }

        }

    } ?>