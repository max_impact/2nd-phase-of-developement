CREATE TABLE `ListingTemplate_Field` (
  `listingtemplate_id` int(11) NOT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldvalues` text COLLATE utf8_unicode_ci NOT NULL,
  `instructions` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `required` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `search` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `searchbykeyword` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `searchbyrange` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `show_order` int(11) NOT NULL DEFAULT '0',
  `enabled` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`listingtemplate_id`,`field`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci