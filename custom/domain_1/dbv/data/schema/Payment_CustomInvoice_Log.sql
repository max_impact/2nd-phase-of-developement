CREATE TABLE `Payment_CustomInvoice_Log` (
  `payment_log_id` int(11) NOT NULL DEFAULT '0',
  `custom_invoice_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `items_price` text COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  KEY `payment_log_id` (`payment_log_id`),
  KEY `custom_invoice_id` (`custom_invoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci