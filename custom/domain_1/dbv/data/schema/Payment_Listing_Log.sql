CREATE TABLE `Payment_Listing_Log` (
  `payment_log_id` int(11) NOT NULL DEFAULT '0',
  `listing_id` int(11) NOT NULL DEFAULT '0',
  `listing_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date NOT NULL DEFAULT '0000-00-00',
  `categories` tinyint(4) NOT NULL DEFAULT '0',
  `extra_categories` tinyint(4) NOT NULL DEFAULT '0',
  `listingtemplate_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  KEY `payment_log_id` (`payment_log_id`),
  KEY `listing_id` (`listing_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci