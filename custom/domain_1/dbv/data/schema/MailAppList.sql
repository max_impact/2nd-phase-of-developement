CREATE TABLE `MailAppList` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `module` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(1) CHARACTER SET utf8 DEFAULT NULL COMMENT 'P=Pending, F=Finished, R=Running, E=Error',
  `filename` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `categories` text CHARACTER SET utf8,
  `progress` int(11) DEFAULT NULL,
  `total_item_exported` int(11) NOT NULL DEFAULT '0',
  `last_account_exported` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci