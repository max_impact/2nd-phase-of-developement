CREATE TABLE `Invoice_Classified` (
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `classified_id` int(11) NOT NULL DEFAULT '0',
  `classified_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  KEY `invoice_id` (`invoice_id`),
  KEY `classified_id` (`classified_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci