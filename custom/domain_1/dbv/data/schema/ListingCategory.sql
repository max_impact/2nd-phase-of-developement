CREATE TABLE `ListingCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root_id` int(11) DEFAULT '0',
  `left` int(11) DEFAULT '0',
  `right` int(11) DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `summary_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `active_listing` int(11) NOT NULL DEFAULT '0',
  `full_friendly_url` text COLLATE utf8_unicode_ci,
  `enabled` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `count_sub` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `active_listing` (`active_listing`),
  KEY `title1` (`title`),
  KEY `friendly_url1` (`friendly_url`),
  KEY `right` (`right`),
  KEY `root_id` (`root_id`),
  KEY `left` (`left`),
  KEY `cat_tree` (`root_id`,`left`,`right`),
  KEY `category_id_2` (`category_id`,`active_listing`),
  FULLTEXT KEY `keywords` (`keywords`,`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci