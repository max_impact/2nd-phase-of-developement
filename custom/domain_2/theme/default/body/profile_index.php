<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /theme/default/body/profile_index.php
	# ----------------------------------------------------------------------------------------------------

    include(system_getFrontendPath("sitecontent_top.php"));
    $hideLabelAbout = true; 
?>

	<div class="row-fluid">

        <div class="span4 flex-box-group color-1">
        	<h2><?=system_showText(LANG_LABEL_ABOUT_ME);?></h2>
            <? include(system_getFrontendPath("socialnetwork/user_info.php")); ?>
        </div>
    
        <div class="span8">
                   
            <? include(system_getFrontendPath("socialnetwork/page_tabs.php")); ?>
            
            <div class="row-fluid">
                <? include(system_getFrontendPath("socialnetwork/user_contents.php")); ?>
            </div>
            
        </div>
        
	</div>

	<? include(system_getFrontendPath("sitecontent_bottom.php")); ?> 