<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /theme/diningguide/body/profile_welcome.php
	# ----------------------------------------------------------------------------------------------------

?>

	<div class="content-full">

        <div class="content">
                    
            <div class="content-center profile">
                <? include(system_getFrontendPath("socialnetwork/welcome.php")); ?>
                <? include(system_getFrontendPath("sitecontent_top.php")); ?>
            </div>
            
        </div>
    	
    </div>