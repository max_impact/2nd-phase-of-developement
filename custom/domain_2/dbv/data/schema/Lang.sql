CREATE TABLE `Lang` (
  `id_number` int(11) NOT NULL AUTO_INCREMENT,
  `id` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang_enabled` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `lang_default` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `lang_order` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_number`),
  KEY `name` (`name`),
  KEY `enabled` (`lang_enabled`),
  KEY `default` (`lang_default`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci