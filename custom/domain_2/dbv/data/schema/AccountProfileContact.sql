CREATE TABLE `AccountProfileContact` (
  `account_id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int(11) NOT NULL,
  `facebook_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_image_height` int(11) NOT NULL,
  `facebook_image_width` int(11) NOT NULL,
  `has_profile` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`account_id`),
  KEY `image_id` (`image_id`,`has_profile`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci