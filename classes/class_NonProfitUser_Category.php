<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_NonProfitUser_Category.php
	# ----------------------------------------------------------------------------------------------------

	class NonProfitUser_Category extends Handle {

		var $id;
		var $account_id;
		var $category_id;
		
		var $category_root_id;
              
		var $category_node_left;
		var $category_node_right;
		
		/*
		 * Dont save this field
		 */
		var $total_listings;
		
		function NonProfitUser_Category($var='') {
			if (is_numeric($var) && ($var)) {
				$dbMain = db_getDBObject(DEFAULT_DB, true);
				if (defined("SELECTED_DOMAIN_ID")) {
					$db = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$db = db_getDBObject();
				}
				unset($dbMain);
				$sql = "SELECT * FROM NonProfitUser_Category WHERE id = $var";
				$row = mysql_fetch_array($db->query($sql));
				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}
		}

		function makeFromRow($row='') {
			if ($row['id']) $this->id = $row['id'];
			else if (!$this->id) $this->id = 0;
			if ($row['account_id']) $this->account_id = $row['account_id'];
			else if (!$this->account_id) $this->account_id = 0;
			if ($row['category_id']) $this->category_id = $row['category_id'];
			else if (!$this->category_id) $this->category_id = 0;
			if ($row['category_root_id']) $this->category_root_id = $row['category_root_id'];
			else if (!$this->category_root_id) $this->category_root_id = 0;
                       	if ($row['category_node_left']) $this->category_node_left = $row['category_node_left'];
			else if (!$this->category_node_left) $this->category_node_left = 0;
			if ($row['category_node_right']) $this->category_node_right = $row['category_node_right'];
			else if (!$this->category_node_right) $this->category_node_right = 0;
		}

		function Save() {
//			$insert_password = $this->password;
			$this->prepareToSave();
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
			unset($dbMain);
			if ($this->id) { 
				$sql  = "UPDATE NonProfitUser_Category SET"
					. " account_id = $this->account_id,"
					. " category_id = $this->category_id,"
					. " category_root_id = $this->category_root_id,"
                                       	. " category_node_left = $this->category_node_left,"
					. " category_node_right = $this->category_node_right,"
					. " WHERE id = $this->id";
//                                echo  $sql; exit;
				$dbObj->query($sql);
			} else {
				$sql = "INSERT INTO NonProfitUser_Category"
					. " (account_id, category_id, category_root_id, category_node_left, category_node_right)"
					. " VALUES"
					. " ($this->account_id, $this->category_id,$this->category_root_id, $this->category_node_left, $this->category_node_right)";
//				echo  $sql; exit;
                                $dbObj->query($sql);
				$this->id = mysql_insert_id($dbObj->link_id);
			}
			$this->prepareToUse();
		}

		function Delete() {
			/**
			* Deleting this object
			**/
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
			unset($dbMain);;
			$sql = "DELETE FROM NonProfitUser_Category WHERE id = $this->id";
			$dbObj->query($sql);
		}

		
	
        
        function getCategoriesByUserID($account_id){
            
            $dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$dbObj = db_getDBObject();
			}
			unset($dbMain);
            
            $sql = "SELECT ListingCategory.title , 
                           Listing_Category.category_id
                      FROM NonProfitUser_Category Listing_Category
                        inner join NonProfitListingCategory ListingCategory on ListingCategory.id = Listing_Category.category_id
                     WHERE Listing_Category.account_id =".$account_id." order by ListingCategory.title";
            $result = $dbObj->query($sql);
            if(mysql_num_rows($result)){
                $i=0;
                while($row = mysql_fetch_assoc($result)){
                    $categories_array[$i]["category_id"] = $row["category_id"];
                    $categories_array[$i]["name"] = $row["title"];
                    $i++;
                }
                return $categories_array;
            }else{
                return false;
            }
            
            
        }

	}

?>
