<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /frontend/socialnetwork/welcome.php
	# ----------------------------------------------------------------------------------------------------

?>

    <h2><?=LANG_CREATE_MEMBER_PROFILE?></h2>
   	
    <div class="content-custom cont_100">
        
        <h2><?=system_showText(LANG_PROFILE_WELCOME);?></h2>
        
        <h3><?=system_showText(LANG_PROFILE_WELCOME2);?> <?=EDIRECTORY_TITLE?>.</h3>
        
        <br />
    </div>