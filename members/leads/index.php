<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /members/leads/index.php
	# ----------------------------------------------------------------------------------------------------

	# ----------------------------------------------------------------------------------------------------
	# LOAD CONFIG
	# ----------------------------------------------------------------------------------------------------
	include("../../conf/loadconfig.inc.php");

	# ----------------------------------------------------------------------------------------------------
	# SESSION
	# ----------------------------------------------------------------------------------------------------
	sess_validateSession();
	$acctId = sess_getAccountIdFromSession();

	$url_redirect = "".DEFAULT_URL."/".MEMBERS_ALIAS."/leads";
	$url_base     = "".DEFAULT_URL."/".MEMBERS_ALIAS."";

	extract($_GET);
	extract($_POST);
	
	if (!$item_type) $item_type = "listing";
    
    if ($item_type == "listing") {
    	$itemObj = new Listing($item_id);
    	$tableName  = "Listing";
    } elseif ($item_type == "event") {
    	$itemObj = new Event($item_id);
    	$tableName  = "Event";
    } elseif ($item_type == "classified") {
    	$itemObj = new Classified($item_id);
    	$tableName  = "Classified";
    }
    
    # ----------------------------------------------------------------------------------------------------
	# CODE
	# ----------------------------------------------------------------------------------------------------
	include(EDIRECTORY_ROOT."/includes/code/lead.php");

	// Page Browsing /////////////////////////////////////////   
	$where .= " Leads.type = '$item_type' AND Leads.type != 'general' AND Leads.item_id = '$item_id' AND Leads.item_id = $tableName.id AND $tableName.account_id = '$acctId' ";

    $pageObj = new pageBrowsing("Leads, $tableName", $screen, RESULTS_PER_PAGE, "entered DESC", "first_name", $letter, $where, "Leads.*");
	$leadsArrTmp = $pageObj->retrievePage("array");
	if ($leadsArrTmp) foreach ($leadsArrTmp as $each_leadssArrTmp) {
		$auxLeadObj = new Lead($each_leadssArrTmp["id"]);
        $leadsArr[] = $auxLeadObj->data_in_array;
	}

	$paging_url = DEFAULT_URL."/".MEMBERS_ALIAS."/leads/index.php?item_id=$item_id&item_screen=$item_screen&item_letter=$item_letter";

	// Letters Menu
	$letters = $pageObj->getString("letters");
	foreach ($letters as $each_letter) {
		if ($each_letter == "#") {
			$letters_menu .= "<a href=\"$paging_url&letter=no\" ".(($letter == "no") ? "style=\"color:#EF413D\"" : "" ).">".string_strtoupper($each_letter)."</a>";
		} else {
			$letters_menu .= "<a href=\"$paging_url&letter=".$each_letter."\" ".(($each_letter == $letter) ? "style=\"color:#EF413D\"" : "" ).">".string_strtoupper($each_letter)."</a>";
		}
	}

	# PAGES DROP DOWN ----------------------------------------------------------------------------------------------
	$pagesDropDown = $pageObj->getPagesDropDown($_GET, $paging_url, $screen, system_showText(LANG_PAGING_GOTOPAGE).": ", "this.form.submit();");
	# --------------------------------------------------------------------------------------------------------------

	# ----------------------------------------------------------------------------------------------------
	# HEADER
	# ----------------------------------------------------------------------------------------------------
	include(MEMBERS_EDIRECTORY_ROOT."/layout/header.php");

	# ----------------------------------------------------------------------------------------------------
	# NAVBAR
	# ----------------------------------------------------------------------------------------------------
	include(MEMBERS_EDIRECTORY_ROOT."/layout/navbar.php");
?>

    <div class="content members">

        <? 
        require(EDIRECTORY_ROOT."/".SITEMGR_ALIAS."/registration.php");
        require(EDIRECTORY_ROOT."/includes/code/checkregistration.php");
        require(EDIRECTORY_ROOT."/frontend/checkregbin.php");
        ?>

        <h2>
            <?=system_showText(LANG_LABEL_LEADS)?>
            <? if ($leadsArrTmp) {
                if ($item_id) { ?> - 
                    <?=($itemObj->getString("title", true))?>
                <? }
            } ?>
        </h2>

        <ul class="list-view">
            <li class="list-back"><a href="javascript:history.back(-1);"><?=system_showText(LANG_LABEL_BACK);?></a></li>
        </ul>

        <?
        include(INCLUDES_DIR."/tables/table_paging.php");
        
        if ($leadsArrTmp) {
            include(INCLUDES_DIR."/tables/table_lead.php");
            
            $bottomPagination = true;
            include(INCLUDES_DIR."/tables/table_paging.php");
        } else {
            echo "<p class=\"informationMessage\">".system_showText(LANG_MSG_NO_RESULTS_FOUND)."</p>";
        }
        ?>

    </div>

<?
	# ----------------------------------------------------------------------------------------------------
	# FOOTER
	# ----------------------------------------------------------------------------------------------------
	include(MEMBERS_EDIRECTORY_ROOT."/layout/footer.php");
?>