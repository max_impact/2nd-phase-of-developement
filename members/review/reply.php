<?
    /*==================================================================*\
    ######################################################################
    #                                                                    #
    # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
    #                                                                    #
    # This file may not be redistributed in whole or part.               #
    # eDirectory is licensed on a per-domain basis.                      #
    #                                                                    #
    # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
    #                                                                    #
    # http://www.edirectory.com | http://www.edirectory.com/license.html #
    ######################################################################
    \*==================================================================*/

    # ----------------------------------------------------------------------------------------------------
    # * FILE: /members/review/index.php
    # ----------------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------------
    # LOAD CONFIG
    # ----------------------------------------------------------------------------------------------------
    include("../../conf/loadconfig.inc.php");

    # ----------------------------------------------------------------------------------------------------
    # UPDATE REPLY
    # ----------------------------------------------------------------------------------------------------
    if(string_strlen(trim($_GET['reply'])) > 0) {

        setting_get("review_approve", $review_approve);
        $responseapproved = 0;
        if (!$review_approve == 'on') $responseapproved = 1;
        
        $reviewObj = new Review($_GET['idReview']);
        $reviewObj->setString("response", trim($_GET['reply']));
        $reviewObj->setString("responseapproved", $responseapproved);
        $reviewObj->save();

        /* send e-mail to sitemgr */
        setting_get("sitemgr_rate_email", $sitemgr_rate_email);
        $sitemgr_rate_emails = explode(",", $sitemgr_rate_email);

        $reviewObj = new Review($_GET['idReview']);
        
        $domain = new Domain(SELECTED_DOMAIN_ID);
        $domain_url = ((SSL_ENABLED == "on" && FORCE_SITEMGR_SSL == "on") ? SECURE_URL : NON_SECURE_URL);
        $domain_url = str_replace($_SERVER["HTTP_HOST"],$domain->getstring("url"),$domain_url);

        // site manager warning message /////////////////////////////////////
        $emailSubject = "[".EDIRECTORY_TITLE."] ".system_showText(LANG_NOTIFY_NEWREPLY);
        $sitemgr_msg = system_showText(LANG_LABEL_SITE_MANAGER).",<br /><br />"
                        ."".system_showText(LANG_NOTIFY_NEWREPLY_1)." <strong>" . $reviewObj->getString('review_title', true) . "</strong> ".system_showText(LANG_NOTIFY_NEWREPLY_2).".</strong> <br /><br />"
                        ."".system_showText(LANG_NOTIFY_NEWREPLY_3).":<br />"
                        ."<a href=\"".$domain_url."/".SITEMGR_ALIAS."/review\" target=\"_blank\">".$domain_url."/".SITEMGR_ALIAS."/review</a><br /><br />"
                    ."</div>
                </body>
            </html>";
        
        system_notifySitemgr($sitemgr_rate_emails, $emailSubject, $sitemgr_msg);
            
        /* */

        if (!$review_approve == 'on') {
        
            /* send e-mail to listing owner */
            if($reviewObj->getString('item_type') == 'listing') {
                $itemObj = new Listing($reviewObj->getNumber('item_id'));
                $contactObj = new Contact($itemObj->getNumber("account_id"));
                if($emailNotificationObj = system_checkEmail(SYSTEM_APPROVE_REPLY)) {
                    setting_get("sitemgr_send_email", $sitemgr_send_email);
                    setting_get("sitemgr_email", $sitemgr_email);
                    $sitemgr_emails = explode(",", $sitemgr_email);
                    if ($sitemgr_emails[0]) $sitemgr_email = $sitemgr_emails[0];
                    $subject   = $emailNotificationObj->getString("subject");
                    $body      = $emailNotificationObj->getString("body");
                    $body      = system_replaceEmailVariables($body, $itemObj->getNumber('id'), 'listing');
                    $subject   = system_replaceEmailVariables($subject, $itemObj->getNumber('id'), 'listing');
                    $body      = html_entity_decode($body);
                    $subject   = html_entity_decode($subject);
                    system_mail($contactObj->getString("email"), $subject, $body, EDIRECTORY_TITLE." <$sitemgr_email>", $emailNotificationObj->getString("content_type"), "", $emailNotificationObj->getString("bcc"), $error);
                }
            }
            
            /* send e-mail to article owner */
            if($reviewObj->getString('item_type') == 'article') {
                $itemObj = new Article($reviewObj->getNumber('item_id'));
                $contactObj = new Contact($itemObj->getNumber("account_id"));
                if($emailNotificationObj = system_checkEmail(SYSTEM_APPROVE_REPLY)) {
                    setting_get("sitemgr_send_email", $sitemgr_send_email);
                    setting_get("sitemgr_email", $sitemgr_email);
                    $sitemgr_emails = explode(",", $sitemgr_email);
                    if ($sitemgr_emails[0]) $sitemgr_email = $sitemgr_emails[0];
                    $subject   = $emailNotificationObj->getString("subject");
                    $body      = $emailNotificationObj->getString("body");
                    $body      = system_replaceEmailVariables($body, $itemObj->getNumber('id'), 'article');
                    $subject   = system_replaceEmailVariables($subject, $itemObj->getNumber('id'), 'article');
                    $body      = html_entity_decode($body);
                    $subject   = html_entity_decode($subject);
                    system_mail($contactObj->getString("email"), $subject, $body, EDIRECTORY_TITLE." <$sitemgr_email>", $emailNotificationObj->getString("content_type"), "", $emailNotificationObj->getString("bcc"), $error);
                }
            }
            /* */

			/* send e-mail to article owner */
            if($reviewObj->getString('item_type') == 'promotion') {
                $itemObj = new Promotion($reviewObj->getNumber('item_id'));
                $contactObj = new Contact($itemObj->getNumber("account_id"));
                if($emailNotificationObj = system_checkEmail(SYSTEM_APPROVE_REPLY)) {
                    setting_get("sitemgr_send_email", $sitemgr_send_email);
                    setting_get("sitemgr_email", $sitemgr_email);
                    $sitemgr_emails = explode(",", $sitemgr_email);
                    if ($sitemgr_emails[0]) $sitemgr_email = $sitemgr_emails[0];
                    $subject   = $emailNotificationObj->getString("subject");
                    $body      = $emailNotificationObj->getString("body");
                    $body      = system_replaceEmailVariables($body, $itemObj->getNumber('id'), 'promotion');
                    $subject   = system_replaceEmailVariables($subject, $itemObj->getNumber('id'), 'promotion');
                    $body      = html_entity_decode($body);
                    $subject   = html_entity_decode($subject);
                    system_mail($contactObj->getString("email"), $subject, $body, EDIRECTORY_TITLE." <$sitemgr_email>", $emailNotificationObj->getString("content_type"), "", $emailNotificationObj->getString("bcc"), $error);
                }
            }
        }

        $message = 3;
        $response = "&class=successMessage&message=".$message;
    } else {
        $message = 4;
        $response = "&class=errorMessage&message=".$message;
    }

    header('Location: ' . DEFAULT_URL . '/'.MEMBERS_ALIAS.'/review/index.php?item_type=' . $_GET['item_type'] . '&item_id=' . $_GET['item_id'] . $response);
?>