<?

/* ==================================================================*\
  ######################################################################
  #                                                                    #
  # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
  #                                                                    #
  # This file may not be redistributed in whole or part.               #
  # eDirectory is licensed on a per-domain basis.                      #
  #                                                                    #
  # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
  #                                                                    #
  # http://www.edirectory.com | http://www.edirectory.com/license.html #
  ######################################################################
  \*================================================================== */

# ----------------------------------------------------------------------------------------------------
# * FILE: /controller/listing/results.php
# ----------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------
# MODULE REWRITE
# ----------------------------------------------------------------------------------------------------
include(EDIR_CONTROLER_FOLDER . "/" . LISTING_FEATURE_FOLDER . "/rewrite.php");

# ----------------------------------------------------------------------------------------------------
# VALIDATION
# ----------------------------------------------------------------------------------------------------
include(EDIRECTORY_ROOT . "/includes/code/validate_querystring.php");
include(EDIRECTORY_ROOT . "/includes/code/validate_frontrequest.php");
//            echo $category_id; exit;
if ($category_id) {
    $catObj = new ListingCategory($category_id);
    if (!$catObj->getString("title")) {
        header("Location: " . LISTING_DEFAULT_URL . "/index.php");
        exit;
    }
}
//             echo $cause_id; exit;
if ($cause_id) {
    $catObj = new NonProfitListingCategory($cause_id);
    if (!$catObj->getString("title")) {
        header("Location: " . LISTING_DEFAULT_URL . "/index.php");
        exit;
    }
}

# ----------------------------------------------------------------------------------------------------------------------
# RESULTS
# ----------------------------------------------------------------------------------------------------------------------

$search_lock = false;
$search_lock_word = false;

if (LISTING_SCALABILITY_OPTIMIZATION == "on") {
    if (!$enable_search_lock) {
        $_GET["id"] = 0;
        $search_lock = true;
    } else {
        if ($_GET["keyword"] && string_strlen($_GET["keyword"]) < (int) FT_MIN_WORD_LEN && !$_GET["where"]) {
            $_GET["id"] = 0;
            $search_lock = true;
            $search_lock_word = true;
        } else if ($_GET["keyword"] && !$_GET["where"]) {
            $aux = explode(" ", $_GET["keyword"]);
            $search_lock = true;
            $search_lock_word = true;
            for ($i = 0; $i < count($aux); $i++) {
                if (string_strlen($aux[$i]) >= (int) FT_MIN_WORD_LEN) {
                    $search_lock = false;
                    $search_lock_word = false;
                }
            }
            if ($search_lock) {
                $_GET["id"] = 0;
            }
        }

        if ($_GET["where"] && string_strlen($_GET["where"]) < (int) FT_MIN_WORD_LEN && !$_GET["keyword"]) {
            $_GET["id"] = 0;
            $search_lock = true;
            $search_lock_word = true;
        } else if ($_GET["where"] && !$_GET["keyword"]) {
            $aux = explode(" ", $_GET["where"]);
            $search_lock = true;
            $search_lock_word = true;
            for ($i = 0; $i < count($aux); $i++) {
                if (string_strlen($aux[$i]) >= (int) FT_MIN_WORD_LEN) {
                    $search_lock = false;
                    $search_lock_word = false;
                }
            }
            if ($search_lock) {
                $_GET["id"] = 0;
            }
        }

        if ($_GET["keyword"] && string_strlen($_GET["keyword"]) < (int) FT_MIN_WORD_LEN && $_GET["where"] && string_strlen($_GET["where"]) < (int) FT_MIN_WORD_LEN) {
            $_GET["id"] = 0;
            $search_lock = true;
            $search_lock_word = true;
        }
    }
}

// replacing useless spaces in search by "where"
if ($_GET["where"]) {
    if (SELECTED_DOMAIN_ID == 3) {
        $diff = explode(',', $_GET['where']);

        $_SESSION['s_city'] = str_replace('+', ' ', $diff['0']);
        $_SESSION['s_state'] = str_replace('+', ' ', $diff['1']);
        $_SESSION['s_country'] = str_replace('+', ' ', $diff['2']);
        $_SESSION['s_state'] = trim($_SESSION['s_state']);
    }
    while (string_strpos($_GET["where"], "  ") !== false) {
        str_replace("  ", " ", $_GET["where"]);
    }
    if ((string_strpos($_GET["where"], ",") !== false) && (string_strpos($_GET["where"], ", ") === false)) {
        str_replace(",", ", ", $_GET["where"]);
    }
}








unset($searchReturn);

if (!$search_lock) {


    if (!($_GET['id'] || $_GET['keyword'])) {

        if ($_GET['letter']) {

            $letter = $_GET['letter'];
        }
    }


    $searchReturn = search_frontListingSearch($_GET, "listing_results");
    $freelevel = getfreelistinlevel();
    $searchReturn["where_clause"] = $searchReturn["where_clause"] . " AND Listing_Summary.level<>" . $freelevel;
 
    if (SELECTED_DOMAIN_ID == 1) {
        $searchReturn["order_by"] = "fbcount DESC";
    }
           if (SELECTED_DOMAIN_ID == 2) {
      zipproximity_getWhereZipCodeProximity($_GET["zip"],$_GET["dist"], $whereZipCodeProximity, $order_by_zipcode_score);
      $output=str_replace("AS zipcode_score", "", $order_by_zipcode_score);
//      print_r($output);exit;
 $searchReturn["order_by"] = $output;
      
     }
    $aux_items_per_page = ($_COOKIE["listing_results_per_page"] ? $_COOKIE["listing_results_per_page"] : 10);

    $pageObj = new pageBrowsing($searchReturn["from_tables"], ($_GET["url_full"] ? $page : $screen), $aux_items_per_page, $searchReturn["order_by"], "Listing_Summary.title", $letter, $searchReturn["where_clause"], $searchReturn["select_columns"], "Listing_Summary", $searchReturn["group_by"]);
//    $listingsarray = $pageObj->retrievePage("array", $searchReturn["total_listings"]);
//    if (SELECTED_DOMAIN_ID == 1) {
////        print_r($listings);exit;
//        if(isset($listingsarray) && $listingsarray != null){
//       foreach ($listingsarray as $listing){
//            $url = "http://" . $_SERVER['HTTP_HOST'] . "/listing/" . $listing['friendly_url'] . ".html";
//
//            include(EDIRECTORY_ROOT . "/fb_share.php");
//            $data2 = mysql_query("UPDATE Listing_Summary  SET fbcount=(" . $fbcount . ")  where friendly_url = ('" . $listing['friendly_url'] . "')");
//        
//            //echo "UPDATE Listing_Summary  SET fbcount=(" . $fbcount . ")  where friendly_url = ('" . $listing['friendly_url'] . "')";
//       }
//    }
//    }
    $listings = $pageObj->retrievePage("array", $searchReturn["total_listings"]);
    $paging_url = LISTING_DEFAULT_URL . "/results.php";
//                echo "<pre>"; print_r($listings);echo "</pre>";
    // the following code for getting all free listings
    $get_requests = $_GET;
    if (!($_GET['id'] || $_GET['keyword'])) {

        if ($_GET['letterb']) {

            $get_requests['letter'] = $letterb = $_GET['letterb'];
        }
    }

    $bronzeSearchReturn = search_frontListingSearch($get_requests, "listing_results");
    $bronzeSearchReturn["where_clause"] = $bronzeSearchReturn["where_clause"] . " AND Listing_Summary.level=" . $freelevel;
//               echo "<pre>";print_r($bronzeSearchReturn);echo "</pre>";exit;
    //                       echo "<pre>";print_r($bronzeSearchReturn["total_listings"]);echo "</pre>";exit;
    $bronzePageObj = new pageBrowsing($bronzeSearchReturn["from_tables"], (MODREWRITE_FEATURE == "on" && $get_requests["url_full"] ? $pageb : $screen), 30, $bronzeSearchReturn["order_by"], "Listing_Summary.title", $letterb, $bronzeSearchReturn["where_clause"], $bronzeSearchReturn["select_columns"], "Listing_Summary", $bronzeSearchReturn["group_by"]);
//            			  echo "<pre>";print_r($bronzePageObj);echo "</pre>";exit;
    $bronze_listings = $bronzePageObj->retrievePage("array", $bronzeSearchReturn["total_listings"]);
//            		echo "<pre>";print_r($bronze_listings);echo "</pre>";exit;
    /*
     * Will be used on:
     * /frontend/results_info.php
     * /frontend/results_filter.php
     * /frontend/results_maps.php
     * functions/script_funct.php
     */
    $aux_module_per_page = "listing";
    $aux_module_items = $listings;
    $aux_module_itemRSSSection = "listing";

    /*
     * Will be used on
     * /frontend/browsebycategory.php
     */

    $aux_CategoryObj = "ListingCategory";
    $aux_CategoryModuleURL = LISTING_DEFAULT_URL;
    $aux_CategoryNumColumn = 3;
    $aux_CategoryActiveField = 'active_listing';

    $levelsWithReview = system_retrieveLevelsWithInfoEnabled("has_review");

    $array_search_params = array();
    $array_search_params_map = array();

    if ($_GET["url_full"]) {
        if ($browsebycategory) {
            $paging_url = LISTING_DEFAULT_URL . "/" . ALIAS_CATEGORY_URL_DIVISOR;
            $aux = str_replace(EDIRECTORY_FOLDER . "/" . ALIAS_LISTING_MODULE . "/" . ALIAS_CATEGORY_URL_DIVISOR . "/", "", $_GET["url_full"]);
        } else if ($browsebycause) {
            $paging_url = LISTING_DEFAULT_URL . "/" . ALIAS_CAUSE_URL_DIVISOR;
            $aux = str_replace(EDIRECTORY_FOLDER . "/" . ALIAS_LISTING_MODULE . "/" . ALIAS_CAUSE_URL_DIVISOR . "/", "", $_GET["url_full"]);
        } else if ($browsebylocation) {
            $paging_url = LISTING_DEFAULT_URL . "/" . ALIAS_LOCATION_URL_DIVISOR;
            $aux = str_replace(EDIRECTORY_FOLDER . "/" . ALIAS_LISTING_MODULE . "/" . ALIAS_LOCATION_URL_DIVISOR . "/", "", $_GET["url_full"]);
        }

        $parts = explode("/", $aux);

        for ($i = 0; $i < count($parts); $i++) {
            if ($parts[$i]) {
                if ($parts[$i] != "page" && $parts[$i] != "letter" && $parts[$i] != "orderby") {
                    $array_search_params[] = "/" . urlencode($parts[$i]);
                } else {
                    if ($parts[$i] != "page" && $parts[$i] != "letter") {
                        $array_search_params[] = "/" . $parts[$i] . "/" . $parts[$i + 1];
                        $i++;
                    } else {
                        $array_search_params_map[] = "/" . $parts[$i] . "/" . $parts[$i + 1];
                        $i++;
                    }
                }
            }
        }

        $url_search_params = implode("/", $array_search_params);

        if (string_substr($url_search_params, -1) == "/") {
            $url_search_params = string_substr($url_search_params, 0, -1);
        }
        $url_search_params = str_replace("//", "/", $url_search_params);

        $url_search_params_map = implode("/", $array_search_params_map);
    } else {

        $paging_url = LISTING_DEFAULT_URL . "/results.php";

        foreach ($_GET as $name => $value) {
            if ($name != "screen" && $name != "letter" && $name != "url_full") {
                if ($name == "keyword" || $name == "where") {
                    $array_search_params[] = $name . "=" . urlencode($value);
                } else {
                    $array_search_params[] = $name . "=" . $value;
                }
            } elseif ($name != "url_full") {
                $array_search_params_map[] = $name . "=" . $value;
            }
        }

        $url_search_params = implode("&amp;", $array_search_params);

        $url_search_params_map = implode("&amp;", $array_search_params_map);
    }

    /*
     * Preparing Pagination
     */
    if (!($_GET['id'] || $_GET['keyword'])) {
        unset($letters_menu);
        $letters_menu = system_prepareLetterToPagination($pageObj, $searchReturn, $paging_url, $url_search_params, $letter, "title", false, false, false, LISTING_SCALABILITY_OPTIMIZATION);

        $array_pages_code = system_preparePagination($paging_url, $url_search_params, $pageObj, $letter, ($_GET["url_full"] ? $page : $screen), $aux_items_per_page, ($_GET["url_full"] ? false : true));
//                 print_r($array_pages_code);exit;
//                    print_r($array_pages_code);exit;
    }
    // free listing pagination.

    if (!($_GET['id'] || $_GET['keyword'])) {
        unset($letters_menu_bronze);
        $letters_menu_bronze = system_prepareLetterToPagination($bronzePageObj, $bronzeSearchReturn, $paging_url, $url_search_params, $letterb, "title", false, false, false, LISTING_SCALABILITY_OPTIMIZATION, 'letterb', '#content-main-bronze');
        $array_pages_code_bronze = system_preparePagination($paging_url, $url_search_params, $bronzePageObj, $letterb, (MODREWRITE_FEATURE == "on" && $_GET["url_full"] ? $pageb : $screen), 30, (MODREWRITE_FEATURE == "on" && $_GET["url_full"] ? false : true), 'letterb', 'pageb', '#content-main-bronze');
    }

    $user = true;
    $showLetter = true;
    $showLetterBronze = true;
    setting_get('commenting_edir', $commenting_edir);
    setting_get("review_listing_enabled", $review_enabled);
    $db = db_getDBObject();
    $sql = "SELECT count(*) as nunberOfReviews FROM Review WHERE item_type = 'listing'";

    $result = $db->unbuffered_query($sql);
    $result = mysql_fetch_assoc($result);
    $numberOfReviews = $result['nunberOfReviews'];

    if ($review_enabled && $commenting_edir && $numberOfReviews) {
        $orderBy = array(LANG_PAGING_ORDERBYPAGE_ALPHABETICALLY, LANG_PAGING_ORDERBYPAGE_POPULAR, LANG_PAGING_ORDERBYPAGE_RATING);
    } else {
        $orderBy = array(LANG_PAGING_ORDERBYPAGE_ALPHABETICALLY, LANG_PAGING_ORDERBYPAGE_POPULAR);
    }

    if (LISTING_ORDERBY_PRICE) {
        array_unshift($orderBy, LANG_PAGING_ORDERBYPAGE_PRICE);
    }

    $orderbyDropDown = search_getOrderbyDropDown($_GET, ($paging_url_mobile ? $paging_url_mobile : $paging_url), $orderBy, system_showText(LANG_PAGING_ORDERBYPAGE) . " ", "this.form.submit();", $parts, false, false, ($paging_url_mobile ? true : false));

    //Prepare tab "Map view"
    $listViewURL = $paging_url;
    if ($_GET["url_full"]) {
        $listViewURL .= ($url_search_params ? "$url_search_params" : "") . ($url_search_params_map ? $url_search_params_map : "");
    } else {
        $listViewURL .= ($url_search_params ? "?" . str_replace("openMap=1", "", $url_search_params) : "") . ($url_search_params_map ? "&" . $url_search_params_map : "");
    }

    setting_get("gmaps_max_markers", $maxMarkers);
    $maxMarkers = ($maxMarkers ? $maxMarkers : GOOGLE_MAPS_MAX_MARKERS);

    $hideTabMap = false;

    if (($array_pages_code["total"] > $maxMarkers) || ($array_pages_code["total"] == 0)) {
        $hideTabMap = true;
        unset($openMap);
        unset($_GET["openMap"]);
    }
}

if (!$listings && !$letter) {
    $showLetter = false;
}
?>
