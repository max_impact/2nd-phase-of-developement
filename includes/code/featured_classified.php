<?

/* ==================================================================*\
  ######################################################################
  #                                                                    #
  # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
  #                                                                    #
  # This file may not be redistributed in whole or part.               #
  # eDirectory is licensed on a per-domain basis.                      #
  #                                                                    #
  # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
  #                                                                    #
  # http://www.edirectory.com | http://www.edirectory.com/license.html #
  ######################################################################
  \*================================================================== */

# ----------------------------------------------------------------------------------------------------
# * FILE: /includes/code/featured_classified.php
# ----------------------------------------------------------------------------------------------------

$numberOfClassifieds = FEATURED_CLASSIFIED_MAXITEMS;
$lastItemStyle = 0;
$specialItem = FEATURED_CLASSIFIED_MAXITEMS_SPECIAL;

$level = implode(",", system_getLevelDetail("ClassifiedLevel"));
if ($level) {
    unset($searchReturn);
    $searchReturn = search_frontClassifiedSearch($_GET, "random");
    if (SELECTED_DOMAIN_ID == 3) {
       if(  $_SESSION["country"]){
            $ctda = "select id from " . _DIRECTORYDB_NAME . ".Location_1  where name='" . $_SESSION['country'] . "'";
          
            $country_id = @mysql_result(mysql_query($ctda), 0);
            
           }
            if(  $_SESSION["state"]){
            $ctda = "select id from " . _DIRECTORYDB_NAME . ".Location_3  where name='" . $_SESSION['state'] . "'and location_1=".$country_id;
        
            $state_id = @mysql_result(mysql_query($ctda), 0);
             }
            if(  $_SESSION["city"]){
           $ctda = "select id from " . _DIRECTORYDB_NAME . ".Location_4  where name='" .  $_SESSION["city"] . "' and location_3=".$state_id ." and location_1=".$country_id;;
        
           $city_id = @mysql_result(mysql_query($ctda), 0);
           
            }
        if (!$country_id) {
            $country_id = -1;
        }
        $searchReturn["where_clause"].=" and Classified.location_1 =" . $country_id;

        if (!$city_id) {
            $city_id = -1;
        }
      //  $searchReturn["where_clause"].=" and Classified.location_4 =" . $city_id;

        if (!$state_id) {
            $state_id = -1;
        }
        $searchReturn["where_clause"].=" and Classified.location_3 =" . $state_id;
    }

    $sql = "SELECT " . $searchReturn["select_columns"] . " FROM " . $searchReturn["from_tables"] . " WHERE " . (($searchReturn["where_clause"]) ? ($searchReturn["where_clause"] . " AND") : ("")) . " (Classified.level IN (" . $level . ")) " . (($searchReturn["group_by"]) ? ("GROUP BY " . $searchReturn["group_by"]) : ("")) . " ORDER BY `random_number` LIMIT " . $numberOfClassifieds . "";
    if (SELECTED_DOMAIN_ID == 3) {
     $sql = "SELECT " . $searchReturn["select_columns"] . " FROM " . $searchReturn["from_tables"] . " WHERE " . (($searchReturn["where_clause"]) ? ($searchReturn["where_clause"] . " AND") : ("")) . " (Classified.level IN (" . $level . ")) " ;

        $row['Zipcode'] = '14445';
        $miles = 100;
        if ($_SESSION["state"] && $_SESSION["city"]) {
            $dataa="select abbreviation from "._DIRECTORYDB_NAME.".Location_3 where id=".$state_id."";
		$st=@mysql_result(mysql_query($dataa),0);
            $sqll = "SELECT Zipcode FROM `zip` WHERE State = '" . $st . "' AND City = '" . $_SESSION["city"] . "' order by Zipcode desc";
               $resultzip = mysql_query($sqll);
            if ($resultzip) {
                $row = mysql_fetch_assoc($resultzip);
            }
        }
        zipproximity_getWhereZipCodeProximity($row['Zipcode'], $miles, $whereZipCodeProximity, $order_by_zipcode_score);
        if ($whereZipCodeProximity) {
            $sql.=" and " . $whereZipCodeProximity . (($searchReturn["group_by"]) ? ("GROUP BY " . $searchReturn["group_by"]) : ("")) . " ORDER BY `random_number` LIMIT " . $numberOfClassifieds . "";
        }
    }
//    echo $sql; exit;
    $highlight_classifieds = db_getFromDBBySQL("classified", $sql);
}

if ($highlight_classifieds) {

    if (CLASSIFIED_SCALABILITY_OPTIMIZATION != "on") {
        $seeAllText = system_showText(LANG_LABEL_VIEW_ALL_CLASSIFIEDS);
        $seeAllTextLink = CLASSIFIED_DEFAULT_URL . "/results.php";
    }

    $count = 0;
    $ids_report_lote = "";
    unset($array_show_classifieds);

    foreach ($highlight_classifieds as $classified) {

        $ids_report_lote .= $classified->getString("id") . ",";

        $lastItemStyle++;

        $array_show_classifieds[$count]["detailLink"] = "" . CLASSIFIED_DEFAULT_URL . "/" . $classified->getString("friendly_url") . ".html";


        unset($imageObj);

        $imageObj = new Image($classified->getNumber((THEME_USE_IMAGE_BIG ? "image_id" : "thumb_id")));
        if ($imageObj->imageExists()) {
            $array_show_classifieds[$count]["image_tag"] = $imageObj->getTag(true, IMAGE_FEATURED_CLASSIFIED_WIDTH, IMAGE_FEATURED_CLASSIFIED_HEIGHT, $classified->getString("title", false), true);
        } else {
            $array_show_classifieds[$count]["image_tag"] = "";
        }

        $array_show_classifieds[$count]["id"] = htmlspecialchars($classified->getNumber("id"));
        $array_show_classifieds[$count]["account_id"] = $classified->getNumber("account_id");
        $array_show_classifieds[$count]["title"] = $classified->getString("title", true);
        $array_show_classifieds[$count]["description"] = $classified->getString("summarydesc", true);
        $array_show_classifieds[$count]["price"] = ($classified->getNumber("classified_price") != "NULL" ? $classified->getNumber("classified_price") : "");

        if (CLASSIFIED_SCALABILITY_OPTIMIZATION != "on") {
            $array_show_classifieds[$count]["categories"] = system_itemRelatedCategories($classified->getNumber("id"), "classified", true);
            $name = socialnetwork_writeLink($classified->getNumber("account_id"), "profile", "general_see_profile");
            if ($name) {
                $array_show_classifieds[$count]["author_string"] = " " . system_showText(LANG_BY) . " " . $name;
            }
        }

        if ($lastItemStyle == $numberOfClassifieds) {
            $itemStyle = "last";
        } elseif ($lastItemStyle == ($specialItem + 1)) {
            $itemStyle = "first";
        } else {
            $itemStyle = "";
        }
        $array_show_classifieds[$count]["itemStyle"] = $itemStyle;

        $count++;
    }

    $ids_report_lote = string_substr($ids_report_lote, 0, -1);
    report_newRecord("classified", $ids_report_lote, CLASSIFIED_REPORT_SUMMARY_VIEW, true);
}
?>