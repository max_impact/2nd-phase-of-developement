<?php

include("./conf/loadconfig.inc.php");
$name=$_POST['user_name'];
$to="webadmin@iconnectedglobal.com";
$from=$_POST['email'];
$nonprofit_name=$_POST['nonprofit_name'];
$body="Non-Profit Name:".$_POST['nonprofit_name']."\n \n".
     "Non-Profit City:".$_POST['nonprofit_city']. "\n \n".
    "Non-Profit State:".$_POST['nonprofit_state']."\n \n".   
    "Non-Profit Zip:".$_POST['nonprofit_zip']."\n \n".
    "Non-Profit Phone: ".$_POST['nonprofit_phone']."\n \n". 
    "Tell us about this Non-Profit:".$_POST['Tell_us'];

if (!$name){
                        $aux_error .= system_showText(LANG_MSG_CONTACT_ENTER_NAME)."\n";
                    }
 if (!$nonprofit_name){
                        $aux_error .= system_showText(LANG_MSG_CONTACT_ENTER_NON_PROFIT_NAME)."\n";
                    }
                    if (!validate_email($to)){
                        $aux_error .= system_showText(LANG_MSG_CONTACT_ENTER_VALID_EMAIL)."\n";
                    }
                    if (!validate_email($from)){
                        $aux_error .= system_showText(LANG_MSG_CONTACT_ENTER_VALID_EMAIL)."\n";
                    }
                    if (!$body){
                        $aux_error .= system_showText(LANG_MSG_CONTACT_TYPE_MESSAGE)."\n";
                    }

                    if (empty($aux_error)) {

                        if (empty($subject)){
                            $subject = $aux_emptySubjectText;
                        } 

                        $body = str_replace("<br />", "", $body);

                        $name = stripslashes(html_entity_decode($name));

                        $body = ucfirst(system_showText(LANG_FROM)).": ".$name."\n\n".system_showText(LANG_LABEL_EMAIL).": ".$from."\n\n"."Suggest Cause".": "."\n\n".$body;

                        $subject = stripslashes(html_entity_decode($subject));
                        $body 	 = stripslashes($body);

                        $subject = "Suggest Cause";


                        $return = system_mail($to, htmlspecialchars_decode($subject), $body, $from, 'text/plain', '', '', $error, '', '', $from);

                        if ($return) {
                            $error["success"] = TRUE;
                            $error["message"] = system_showText(LANG_CONTACTMSGSUCCESS);
                        }	else {
                            $error["success"] = FALSE;
                            $error = system_showText(LANG_CONTACTMSGFAILED).($error ? '\n'.$error : '')."\n";
                    }

}else{
                         $error["message"]= $aux_error;  
                        }

  header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
                    header("Cache-Control: no-store, no-cache, must-revalidate");
                    header("Cache-Control: post-check=0, pre-check=0", FALSE);
                    header("Pragma: no-cache");
                    header("Content-Type: application/json; charset=".EDIR_CHARSET, TRUE);
                    echo json_encode($error);    
                    exit;
?> 

