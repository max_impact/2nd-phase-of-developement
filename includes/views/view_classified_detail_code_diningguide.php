<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/views/view_classified_detail_code_diningguide.php
	# ----------------------------------------------------------------------------------------------------

    include(INCLUDES_DIR."/views/view_detail_tabs.php");
    
?>	

    <div class="content-main">
        
        <div class="tab-container">
            
            <div id="content_overview" class="tab-content">
                
                <div class="row-fluid top-info">
                    
                    <div class="span10">
                        <h2><?=$classified_title;?></h2>
                    </div>
                    
                    <div class="span2 share">
                        <?=$classified_icon_navbar?>
                    </div>
                    
                </div>
                
                <div class="row-fluid">
                    <? if ($classified_category_tree) { ?>
                        <?=$classified_category_tree?>
                    <? } ?>
                </div>
                
                <div class="row-fluid middle-info">
                    
                    <? if ($imageTag || $classifiedGallery) { ?>
                    
                    <div class="span6">

                        <? if (($imageTag && !$classifiedGallery && $onlyMain) || ($tPreview && $imageTag)) { ?>
                            <div class="image">
                                <?=$imageTag?>
                            </div>
                        <? } ?>

                        <? if ($classifiedGallery) { ?>
                            <div <?=$tPreview ? "class=\"ad-gallery gallery\"" : ""?>>
                                <?=$classifiedGallery?>
                            </div>
                        <? } ?>
                    </div>
                    
                    <? } ?>
                    
                    <div class="span6">
                        <? if ($classified_price != 'NULL' && $classified_price != '') { ?>
                        
                            <p><strong><?=system_showText(LANG_LABEL_PRICE);?>:</strong></p>
                            
                            <p><?=CURRENCY_SYMBOL." ".$classified_price;?></p>
                            
                            <br class="clear" />
                        <? } ?>

                        <? if (($location) || ($classified_address) || ($classified_address2)) { ?>
                        
                            <p><strong><?=system_showText(LANG_LABEL_ADDRESS);?>:</strong></p>

                            <address>
                        
                        <? } ?>

                        <? if ($classified_address) { ?>
                            <span><?=nl2br($classified_address)?></span>
                        <? } ?>

                        <? if ($classified_address2) { ?>
                            <span><?=nl2br($classified_address2)?></span>
                        <? } ?>

                        <? if ($location) { ?>
                            <span><?=$location?></span>
                        <? } ?>

                        <? if (($location) || ($classified_address) || ($classified_address2)) { ?>
                            </address> 
                        <? } ?>

                        <? if ($classified_contactName) { ?>
                            <p><strong><?=system_showText(LANG_LABEL_CONTACTNAME)?>:</strong> <?=nl2br($classified_contactName)?></p>
                        <? } ?>

                        <? if ($classified_phone) { ?>
                            <p><strong><?=system_showText(LANG_LABEL_PHONE)?>:</strong> <?=nl2br($classified_phone)?></p>
                        <? } ?>

                        <? if ($classified_fax) { ?>
                            <p><strong><?=system_showText(LANG_LABEL_FAX)?>:</strong> <?=nl2br($classified_fax)?></p>
                        <? } ?>

                        <? if ($classified_email) { ?>
                            <p><strong><?=system_showText(LANG_LABEL_EMAIL)?>:</strong> <a rel="nofollow" href="<?=$contact_email?>" class="<?=!$tPreview? "fancy_window_tofriend": "";?>" style="<?=$contact_email_style?>"><?=system_showText(LANG_SEND_AN_EMAIL);?></a></p>
                        <? } ?>	

                        <? if ($classified_url) { ?>
                            <? if ($user) { ?>
                                <p><strong><?=system_showText(LANG_LABEL_URL)?>:</strong> <a href="<?=nl2br($classified_url)?>" target="_blank"><?=nl2br($classified_url)?></a></p>
                            <? } else { ?>
                                <p><strong><?=system_showText(LANG_LABEL_URL)?>:</strong> <a href="javascript:void(0);" style="cursor:default"><?=nl2br($classified_url)?></a></p>
                            <? } ?>
                        <? } ?>

                    </div>

                </div>
                
                <div class="row-fluid">
                    
                    <? if ($classified_description) { ?>
                        <div class="content-box">
                            <h2><?=system_showText(LANG_LABEL_DESCRIPTION);?></h2>
                            <p class="long"><?=$classified_description?></p>
                        </div>
                    <? } ?>	
                    
                </div>
                
            </div>
            
        </div>
        
    </div>