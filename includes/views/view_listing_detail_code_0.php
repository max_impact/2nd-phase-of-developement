<?
/* ==================================================================*\
  ######################################################################
  #                                                                    #
  # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
  #                                                                    #
  # This file may not be redistributed in whole or part.               #
  # eDirectory is licensed on a per-domain basis.                      #
  #                                                                    #
  # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
  #                                                                    #
  # http://www.edirectory.com | http://www.edirectory.com/license.html #
  ######################################################################
  \*================================================================== */

# ----------------------------------------------------------------------------------------------------
# * FILE: /includes/views/view_listing_detail_code_0.php
# ----------------------------------------------------------------------------------------------------

include(INCLUDES_DIR . "/views/view_detail_tabs.php");
//print_r($listing);exit;
?>
<div class="tab-container">

    <div id="content_overview" class="tab-content">

        <div class="row-fluid">

            <div class="top-info">

                <h3><?= $listingtemplate_title ?></h3>

                <div class="row-fluid">

                    <div class="span8">
                        <? if ($listingtemplate_summary_review) { ?>
                            <?= $listingtemplate_summary_review; ?>
                        <? } ?>
                    </div>

                    <div class="span4 <?= ($listingtemplate_summary_review ? "text-right" : "text-left") ?>">
                        <?php
                        if (SELECTED_DOMAIN_ID == 1) {
                            $freelevel = getfreelistinlevel();
                            if ($listing->getNumber("level") != $freelevel) {
                                $get_url = "/frontend/view_fb_count.php";
                                $title = $listing->title;
								$list_det['id']=$listing->id;
                                $data = mysql_query("select * from  Listing_Summary WHERE `id`=" . $listing->id);
                                $row = mysql_fetch_assoc($data);
                                $fbc = $row['fbcount'];

                                $url = "http://" . $_SERVER['HTTP_HOST'] . "/listing/" . $listing->friendly_url . ".html";
                                $summary = $listing->description;
                                ?>
                                <div class="span2 Votenowlisting" id="Votenow" style="margin-bottom:10%">
                                    <div class='VotenowParnetlisting'>

                                        <a onClick="fbcall($(this),<?php echo $list_det['id'] ?>);
                                                        return false;" class='Votenowlisting' href="">
                                            <img pagespeed_url_hash="1506468634" src="http://toplocalrated.com/theme/default/images/imagery/Megaphone.png">
                                            Vote For Us!

                                        </a>

                                        <span style="color:#EFB653 ;border: 1px solid #EEEEEE; font-size: 9pt;
                                              margin-left: 10px;
                                              padding: 5px 10px;" id="facebaook_count"> <?php echo $fbc; ?> </span>
                                    </div>
                                </div>
<!-- ======================================= fb vote script ============================= -->
<div id="fb-root"></div>
                                <script>
	window.fbAsyncInit = function() {
    FB.init({
        appId   : '1412617082285011',
        oauth   : true,
        status  : true, // check login status
        cookie  : true, // enable cookies to allow the server to access the session
        xfbml   : true // parse XFBML
    });

  };

function fbcall(x,y){
    FB.login(function(response) {

        if (response.authResponse) {
            console.log('Welcome!  Fetching your information.... ');
            //console.log(response); // dump complete info
            access_token = response.authResponse.accessToken; //get access token
            user_id = response.authResponse.userID; //get FB UID

            FB.api('/me', function(response) {
                user_email = response.email; //get user email
				//console.log(user_email);
          // you can store this data into your database 
		  var get_url = "<?php echo "http://" . $_SERVER['HTTP_HOST'] . $get_url; ?>";
          var detailLink = "<?php echo $listing->friendly_url ?>";
          var facebaook_count = "<?php echo $fbc ?>";
          var url = "<?php echo 'http://' . $_SERVER['HTTP_HOST'] ?>/listing/" + detailLink + ".html";
		 // console.log(url);
		  $.ajax({
						type: "POST",
						url: "../../myvote.php",
						data: { uemail: user_email, location: url }
						})
					.done(function( msg ) {
						//alert( "Data Saved: " + msg );
						//alert(msg);
						if(msg=="1")
				{
					$.get(get_url, {
            facebaook_count: facebaook_count,
            friendly_url: detailLink,
        }, function(data) {
            if (data == 1) {
                newfb = parseInt(facebaook_count, 10) + parseInt('1', 10);

                x.parent(".VotenowParnetlisting").find("#facebaook_count").text(newfb);
            }

        });
								 alert("Thank You For Voting! Please Leave Us A Review On Facebook!");
								  $( document ).ready(function() {
                 //console.log( "ready!" );
				 $.fancybox({
                                    'href': "#dialogdiv" + y,
                                            'width': 1000,
                                            'height': 1000,
                                    });
				///////////////////////
                    });
				}
				else if(msg=="2")
				{
				alert("You Have Already Voted For This Business.  Feel Free To Leave Us A Review on Facebook!");
				$( document ).ready(function() {
                 //console.log( "ready!" );
				 $.fancybox({
                                    'href': "#dialogdiv" + y,
                                            'width': 1000,
                                            'height': 1000,
                                    });
				///////////////////////
                    });
				}
						
            });
 });
        } else {
            //user hit cancel button
            console.log('User cancelled login or did not fully authorize.');

        }
    }, {
        scope: 'publish_stream,email'
    });
}
(function() {
    var e = document.createElement('script');
    e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
    e.async = true;
    document.getElementById('fb-root').appendChild(e);
}());
</script>
<!-- ======================================= end of fb script ================================================ -->
                                <!-- Each listing comments  -->
                                <div class="dialog dialogdiv" id="dialogdiv<?php echo $listing->id ?>" style="display:none" title="Basic dialog">
                                    <div class="fb-comments" data-href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/listing/share/<?php echo $listing->friendly_url ?>.html" data-numposts="5" data-colorscheme="light"></div>
                                </div>
                                <!-- ----------->
                                <?php
                            }
                        }
                        ?>
                        <?= $listingtemplate_icon_navbar ?>
                        <? if ($listingtemplate_twilioClickToCall) { ?>
                            <span class="button-call"><a rel="nofollow" href="<?= $listingtemplate_twilioClickToCall ?>" <?= $twilioClickToCall_style ?>><img src="<?= DEFAULT_URL ?>/images/icon-call-phone.png" title="<?= system_showText(LANG_LABEL_CLICKTOCALL); ?>"/></a></span>
                        <? } ?>

                        <? if ($listingtemplate_twilioSMS) { ?>
                            <span class="button-send"><a rel="nofollow" href="<?= $listingtemplate_twilioSMS ?>" <?= $twilioSMS_style ?>><img src="<?= DEFAULT_URL ?>/images/icon-send-phone.png" title="<?= system_showText(LANG_LABEL_SENDPHONE); ?>"/></a></span>
                        <? } ?>
                    </div>

                </div>

            </div>

        </div>

        <div class="row-fluid">

            <? if ($listingtemplate_category_tree) { ?>
                <div class="span12 top-info">
                    <?= $listingtemplate_category_tree ?>
                </div>
            <? } ?>

        </div>

        <div class="row-fluid middle-info">

            <? if ($listingtemplate_image || $listingtemplate_gallery || $listingtemplate_features) { ?>

                <div class="span7">

                    <? if (($listingtemplate_image && !$listingtemplate_gallery && $onlyMain) || ($tPreview && $listingtemplate_image)) { ?>
                        <div class="image-shadow">
                            <div class="image">
                                <?= $listingtemplate_image ?>
                            </div>
                        </div>
                    <? } ?>

                    <? if ($listingtemplate_gallery) { ?>
                        <div <?= $tPreview ? "class=\"ad-gallery gallery\"" : "" ?>>
                            <?= $listingtemplate_gallery ?>
                        </div>
                    <? } ?>

                    <div <?= ($onlyMain && !$isNoImage ? "class=\"detailfeatures\"" : "") ?>>

                        <? if ($listingtemplate_features) { ?>

                            <br />
                            <div class="well-top"><?= system_showText(LANG_LABEL_GOODKNOW); ?></div>

                            <div class="well-small">
                                <ul>
                                    <ol><?= system_showText(LANG_LABEL_FEATURES); ?></ol>
                                    <ol>
                                        <ul><?= $listingtemplate_features; ?></ul>         
                                    </ol>
                                </ul>
                            </div>

                        <? } ?>
                        <?php
                        if (SELECTED_DOMAIN_ID == 3) {
                            if ($listing)
                                $nonprofit_categories = $listing->getNonProfitCategories(false, false, $listing->getNumber("id"), true, true);

                            if ($nonprofit_categories) {

                                for ($i = 0; $i < count($nonprofit_categories); $i++) {
                                    $arr_return_categories1[] = $nonprofit_categories[$i]->getString("title");
                                }
                                if ($arr_return_categories1)
                                    $return_categories_nonprofit = implode(",", $arr_return_categories1);
                            }
                            ?>
                            <div class="summary-contact <?= (($listingtemplate_designations) ? "span9" : "span12" ) ?>" style="color: #B55CFC">

                                <? if ($return_categories_nonprofit) { ?>
                                    <div style="font-size:15px">
                                        Supports: <?= $return_categories_nonprofit ?>
                                    </div>
                                <? } ?>


                            </div> 
                        <?php } ?>

                        <? if ($listingtemplate_designations) { ?>            
                            <?= $listingtemplate_designations ?>
                        <? } ?>

                    </div>

                </div>

            <? } ?>

            <div class="span5">

                <? if (($listingtemplate_address) || ($listingtemplate_address2) || ($listingtemplate_location)) { ?>
                    <strong><?= system_showText(LANG_LABEL_ADDRESS); ?>:</strong>
                <? } ?>

                <? if (($listingtemplate_address) || ($listingtemplate_address2) || ($listingtemplate_location)) echo "\n<address>\n"; ?>

                <? if ($listingtemplate_address) { ?>
                    <span><?= $listingtemplate_address ?></span>
                <? } ?>

                <? if ($listingtemplate_address2) { ?>
                    <br /><span><?= $listingtemplate_address2 ?></span>
                <? } ?>

                <? if ($listingtemplate_location) { ?>
                    <br /><span><?= $listingtemplate_location ?></span>
                <? } ?>

                <? if (($listingtemplate_address) || ($listingtemplate_address2) || ($listingtemplate_location)) echo "</address>\n"; ?>

                <? if ($listingtemplate_phone) { ?>
                    <strong><?= system_showText(LANG_LISTING_LETTERPHONE) ?>:</strong>
                    <address><?= $listingtemplate_phone ?></address>
                <? } ?>


                <? if ($listingtemplate_fax) { ?>
                    <strong><?= system_showText(LANG_LISTING_LETTERFAX) ?>:</strong>
                    <address><?= $listingtemplate_fax ?></address>
                <? } ?>

                <? if ($listingtemplate_url) { ?>
                    <strong><?= system_showText(LANG_LISTING_LETTERWEBSITE) ?>:</strong>
                    <address class="website"><?= $listingtemplate_url ?></address>
                    <a onclick="javascript:window.open('http://maps.google.com/maps?q=<?php echo $listing->latitude ?>,<?php echo $listing->longitude ?>', 'popup', '')" href="javascript:void(0);">Get driving directions</a>
                    <?
                }



                if ($listingtemplate_claim) {
                    ?>
                    <hr><?= $listingtemplate_claim ?><br />
                <? } ?>

                <? if ($listingtemplate_attachment_file) { ?>
                    <hr><?= $listingtemplate_attachment_file; ?><br />
                <? } ?>

                <? if ($listingtemplate_email) { ?>
                    <a rel="nofollow" href="<?= $listingtemplate_email_link; ?>" class="<?= ($user ? "fancy_window_tofriend" : "" ) ?> btn btn-large btn-success" <?= (!$user ? "style=\"cursor:default;\"" : ""); ?>>
                        <?= system_showText(LANG_LISTING_CONTACT); ?>
                    </a>
                <? } ?>

            </div>

        </div>

        <? if ($listingtemplate_long_description || $listingtemplate_hours_work || $listingtemplate_locations) { ?>

            <div class="row-fluid">

                <div class="content-box">

                    <h4><?= system_showText(LANG_LABEL_DESCRIPTION); ?></h4>
                    <p class="long">
                        <?= $listingtemplate_long_description ? $listingtemplate_long_description . "<br /><br />" : "" ?>
                        <?= $listingtemplate_hours_work ? $listingtemplate_hours_work . "<br /><br />" : "" ?>
                        <?= $listingtemplate_locations ? $listingtemplate_locations : "" ?>
                    </p>

                </div>

            </div>

        <? } ?>

        <? if ($templateExtraFields) { ?>

            <div class="row-fluid">
                <div class="extra-fields">
                    <?= $templateExtraFields; ?>
                </div>
            </div>

        <? } ?>

        <? if ($listingtemplate_review) { ?>

            <div class="flex-box-group color-3 helpful-reviews">

                <h2>
                    <i class="icon-pencil"></i>
                    <?= system_showText(LANG_LABEL_HELPFUL_REVIEWS); ?>

                    <? if (count($reviewsArr) > 3) { ?>
                        <a rel="nofollow" class="view-more" href="javascript:void(0);" <?= (!$user ? "style=\"cursor:default;\"" : "onclick=\"loadReviews('listing', $listingtemplate_id, 1); showTabDetail('review', true);\""); ?>><?= str_replace("[x]", count($reviewsArr), system_showText(LANG_LABEL_SHOW_REVIEWS)); ?></a>
                    <? } else { ?>
                        <a rel="nofollow" class="view-more <?= $class; ?>" href="<?= ($user ? $linkReviewFormPopup : "javascript:void(0);"); ?>"><?= system_showText(LANG_REVIEWRATEIT); ?></a>
                    <? } ?>
                </h2>

                <?= $listingtemplate_review ?>

            </div>

        <? } ?>

    </div>

    <? if ($listingtemplate_review) { ?>

        <div id="content_review" class="tab-content hidden-phone" <?= $activeTab == "review" ? "" : "style=\"display: none;\""; ?>>

            <div class="row-fluid">

                <div class="span12 top-info">

                    <div class="span8">

                        <h3><?= $listingtemplate_title ?></h3>
                        <div class="stars-rating">
                            <div class="rate-<?= (is_numeric($rate_avg) ? $rate_avg : "0") ?>"></div>
                        </div>

                    </div>

                    <div class="span4">
                        <a rel="nofollow" class="btn-review btn btn-success <?= $class; ?>" href="<?= ($user ? $linkReviewFormPopup : "javascript:void(0);"); ?>"><?= system_showText(LANG_REVIEWRATEIT); ?></a>
                    </div>

                </div>

            </div>

            <div id="loading_reviews">
                <img src="<?= DEFAULT_URL . "/theme/" . EDIR_THEME . "/images/iconography/icon-loading-location.gif" ?>" alt="<?= system_showText(LANG_WAITLOADING) ?>"/>
            </div>

            <div id="all_reviews" class="row-fluid"></div>

        </div>

    <? } ?>

    <? if ($listingtemplate_video_snippet) { ?>

        <div id="content_video" class="tab-content hidden-phone" <?= $activeTab == "video" ? "" : "style=\"display: none;\""; ?>>

            <div class="row-fluid">

                <div class="span12 top-info">
                    <h3><?= $listingtemplate_title ?></h3>
                </div>

                <div class="video">
                    <script language="javascript" type="text/javascript">
                                            //<![CDATA[
                                            document.write("<?= str_replace("\"", "'", $listingtemplate_video_snippet) ?>");
                                            //]]>
                    </script>
                </div>

                <? if ($listingtemplate_video_description) { ?>
                    <p><?= nl2br($listingtemplate_video_description); ?></p>
                <? } ?>

            </div>

        </div>

    <? } ?>

    <? if ($hasDeal) { ?>

        <div id="content_deal" class="tab-content" <?= $activeTab == "deal" ? "" : "style=\"display: none;\""; ?>>

            <div class="row-fluid">

                <div class="span12 top-info">
                    <h3><?= $deal_name; ?></h3>
                </div>
            </div>

            <div class="row-fluid">

                <div class="image img-polaroid">
                    <div class="no-link">
                        <?= $imageTag; ?>
                    </div>
                </div>

            </div>

            <div class="row-fluid">

                <? if ($deal_description) { ?>

                    <div class="span6">

                        <div class="description">

                            <p><?= nl2br($deal_description); ?></p>

                        </div>

                    </div>

                <? } ?>

                <div class="<?= ($deal_description ? "span6" : "span12") ?>">

                    <div class="action">

                        <div class="deal-value">
                            <span><?= system_showText(DEAL_VALUE) ?><strong><?= $deal_real_value; ?></strong></span>
                            <span><?= system_showText(LANG_SITEMGR_DISCOUNT) ?><strong> <?= $deal_offer; ?></strong></span>
                            <span><?= system_showText(LANG_LABEL_PROMOTION_PAY) ?><strong><?= $deal_value . ($deal_cents ? $deal_cents : "") ?></strong></span>
                        </div>

                        <? if (!$dealsDone) { ?>

                            <div class="facebookConnect hidden-phone">

                                <? if ($redeemLink) { ?>
                                    <div <?= $buttomClass; ?>>
                                        <h2>
                                            <?
                                            $linkFBRedeem = "<a rel=\"nofollow\" href=\"" . $redeemLink . "\" " . (FACEBOOK_APP_ENABLED != "on" ? "class=\"$linkRedeemClass\"" : "") . " $promotionStyle>" . addslashes($buttonText) . "</a>";
                                            ?>
                                            <script language="javascript" type="text/javascript">
                                                //<![CDATA[
                                                document.write('<?= $linkFBRedeem ?>');
                                                //]]>
                                            </script>
                                        </h2>
                                    </div>
                                    <?
                                }

                                if ($linkText) {
                                    ?>
                                    <p class="redeem-option">
                                        <a rel="nofollow" class="<?= $linkRedeemClass ?>" href="<?= $redeemWFB; ?>" <?= $promotionStyle ?>><?= $linkText; ?></a>
                                    </p>
                                <? } ?>

                            </div>

                            <?
                        }

                        if ($_SESSION["ITEM_ACTION"] == "redeem" && $_SESSION["ITEM_TYPE"] && (is_numeric($_SESSION["ITEM_ID"]) && $_SESSION["ITEM_ID"] == htmlspecialchars($promotion->getNumber('id'))) && sess_isAccountLogged()) {
                            ?>

                            <a href="<?= $_SESSION["fb_deal_redirect"] ? $_SESSION["fb_deal_redirect"] : $linkRedeem; ?>" id="redeem_window" class="fancy_window_iframe" style="display:none"></a>

                            <script type="text/javascript">
                                //<![CDATA[                               
                                $("a.fancy_window_iframe").fancybox({
                                    maxWidth: 475,
                                    padding: 0,
                                    margin: 0,
                                    closeBtn: false,
                                    type: 'iframe'
                                });

                                $(document).ready(function() {
                                    showTabDetail('deal');
                                    $("#redeem_window").trigger('click');
                                });
                                //]]>
                            </script>

                            <?
                            unset($_SESSION["ITEM_ACTION"], $_SESSION["ITEM_TYPE"], $_SESSION["ITEM_ID"], $_SESSION["ACCOUNT_REDIRECT"], $_SESSION["fb_deal_redirect"]);
                        }
                        ?>

                    </div>

                    <div class="deal-timeleft">

                        <? if (!$dealsDone) { ?>

                            <h5><i class="icon-time"></i> <?= system_showText(LANG_LABEL_PROMOTION_TIMELEFT); ?></h5>

                            <h4 id="timeLeft"></h4>

                        <? } else { ?>

                            <h4 class="deal-soldout"><?= system_showText(DEAL_SOLDOUT); ?></h4>

                        <? } ?>

                    </div>

                    <div class="deal-dealsleft">
                        <span><?= system_showText(LANG_LABEL_DEAL_LEFT); ?>: <i id="updateDealsLeft"><?= $deal_left; ?></i></span>
                        <span><?= system_showText(LANG_LABEL_DEAL_BOUGHT); ?>: <i id="updateDeals"><?= $deal_sold; ?></i></span>
                    </div>

                    <? if ($deal_conditions) { ?>
                        <h6><?= ($deal_name . " - " . system_showText(LANG_LABEL_DEAL_CONDITIONS)); ?></h6>
                        <p><?= nl2br($deal_conditions); ?></p>
                    <? } ?>

                </div>

            </div>

        </div>

    <? } ?>

</div>
<?php

                               
$ur = 'https://developers.facebook.com/tools/debug/og/echo?q=http://' . $_SERVER['HTTP_HOST'] . '/listing/' . $listing->friendly_url . '.html';

$response = file_get_contents($ur);

?>
<script>

/*

    function fbcall(x) {

        var get_url = "<?php echo "http://" . $_SERVER['HTTP_HOST'] . $get_url; ?>";

        var detailLink = "<?php echo $listing->friendly_url ?>";
        var facebaook_count = "<?php echo $fbc ?>";
        var url = "<?php echo 'http://' . $_SERVER['HTTP_HOST'] ?>/listing/" + detailLink + ".html";


        var accToken = "";
        FB.init({
            appId: '1412617082285011',
            status: true,
            cookie: false,
            xfbml: true,
            oauth: true
        });
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                var uid = response.authResponse.userID;
                accToken = response.authResponse.accessToken;


                FB.api('https://graph.facebook.com/' + uid + '/edirectorytoplocal:vote?access_token=' + accToken, 'post', {listing:url+'&fbrefresh=1' }, function(response) {
                    var msg = 'Error occured';
                    if (!response || response.error) {
                        if (response.error) {
                            msg += "\n\nType: " + response.error.type + "\n\nMessage: " + response.error.message;
                        }
                        alert(msg);
                    }
                    else {
                        $.fancybox({
                            'href': "#dialogdiv<?php echo $listing->id ?>",
                            'width': 1000,
                            'height': 1000,
                        });
                        // alert('Post was successful! Action ID: ' + response.id);
                    }
                });
            } else {
                FB.login();
            }
        });

        $.get(get_url, {
            facebaook_count: facebaook_count,
            friendly_url: detailLink,
        }, function(data) {
            if (data == 1) {
                newfb = parseInt(facebaook_count, 10) + parseInt('1', 10);

                x.parent(".VotenowParnetlisting").find("#facebaook_count").text(newfb);
            }

        });
//var accToken =FB.getAuthResponse()['accessToken'];


        /*   window.fbAsyncInit = function() {
         FB.init({
         appId: '1412617082285011',
         status: true,
         cookie: false,
         xfbml: true,
         oauth: true
         });
         FB.ui({method: 'feed', message:"",name: "<?php echo $listingtemplate_title ?>", display: 'popup', link: '<?php echo $url ?>', description: "<?php !empty($summary) ? $summary : '' ?>"},
         function(response) {
         console.log(response);
         if (response && response.post_id) {
         
         
         
         } else {
         
         
         }
         }
         );
         
         }
         (function(d) {
         var js, id = 'facebook-jssdk';
         if (d.getElementById(id)) {
         return;
         }
         js = d.createElement('script');
         js.id = id;
         js.async = true;
         js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
         d.getElementsByTagName('head')[0].appendChild(js);
         }(document));
         */
   // }


</script>
<style>

    .VotenowParnetlisting {
        margin-left: -149px;
        width: 220px;
        width:220px;
    } 
    .VotenowParnetlisting .Votenowlisting {
        background-image: -ms-linear-gradient(top, #B8E229 0%, #93C100 100%);
        background-image: -moz-linear-gradient(top, #B8E229 0%, #93C100 100%);
        background-image: -o-linear-gradient(top, #B8E229 0%, #93C100 100%);
        background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #B8E229), color-stop(1, #93C100));
        background-image: -webkit-linear-gradient(top, #B8E229 0%, #93C100 100%);
        background-image: linear-gradient(to bottom, #B8E229 0%, #93C100 100%);

        border-radius: 5px 5px 5px 5px;
        color: #FFFFFF !important;
        font-size: 9pt;
        height: 39px;
        padding: 10px 18px;
        width: 117px;
    }

</style>