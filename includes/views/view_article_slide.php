


<?php


/////////////////////////////////MAP///////////////////////////////////

$auxKey = "";
$googleSettingObj = new GoogleSettings(GOOGLE_MAPS_SETTING, $_SERVER["HTTP_HOST"]);

/* key for demodirectory.com */
if (DEMO_LIVE_MODE) {
    $googleMapsKey = GOOGLE_MAPS_APP_DEMO;
} else {
    $googleMapsKey = $googleSettingObj->getString("value");
}

if ($googleMapsKey) {
    $auxKey = "&amp;key=" . $googleMapsKey;
}
?> 

<div style="">
    <div id="map-canvas" style="height:300px;width:300px;">
    </div>
    <script src="http://maps.google.com/maps/api/js?sensor=false<?php $auxKey ?>" type="text/javascript"></script>


    <!-- ////////////////////////////////////////////////////////// -->

    <div style="margin-top: 20px;"><div style="text-align: center;">Customer Recommendations:</div><br /><? include(system_getFrontendPath("detail_fbcomments.php", "frontend", false, ARTICLE_EDIRECTORY_ROOT)); ?>
<? include(system_getFrontendPath("detail_socialbuttons.php", "frontend", false, ARTICLE_EDIRECTORY_ROOT)); ?>

    </div>

</div>


<script type="text/javascript">

    var LocationData = [
<?php foreach ($LocationData as $data) { ?>
            [<?php echo $data[0]; ?>, <?php echo $data[1]; ?>, "<?php echo $data[2]; ?>","<?php echo $data[3]; ?>" ],
<?php } ?>

    ];

    var mapOptions = {
//          center: new google.maps.LatLng(-34.397, 150.644),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map =
            new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    var bounds = new google.maps.LatLngBounds();
    var infowindow = new google.maps.InfoWindow();
    var alparray = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    var j = 0; var marker=[];var infowindow=[];
    for (var i in LocationData) {


        var p = LocationData[i];
//        alert(p[3])
        var latlng = new google.maps.LatLng(p[1], p[0]);
        bounds.extend(latlng);

        marker[i] = new google.maps.Marker({
            position: latlng,
            map: map,
            title: p[3],
            <?php if(SELECTED_DOMAIN_ID!=3){ ?>
            icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + alparray[j] + '|F7584F|000000'
            <?php }else{ ?>
            icon: 'http://dev.purposeunited.com/images/chart_1.png'
    
          <?php  } ?>
        });
        
        
        listen(i, p);
        
       
        j++;
    }
    
    function listen(i, p) {
        infowindow[i] = new google.maps.InfoWindow({
            content: p[3] + '<br /><a onclick="javascript:callMapLink(' + p[1] + ', ' + p[0] + ')" href="javascript:void(0);">Get driving directions</a>'

        });
        google.maps.event.addListener(marker[i], 'click', function() {
       
//                             infowindow.setContent(this.title);
            infowindow[i].open(map, this);

            /////


            /////

        });
    }

    map.setCenter(bounds.getCenter());
    function callMapLink(p1, p0) {
        window.open("http://maps.google.com/maps?q=" + p1 + "," + p0);
    }
</script> 
