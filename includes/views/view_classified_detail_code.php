<?

/*==================================================================*\
######################################################################
#                                                                    #
# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
#                                                                    #
# This file may not be redistributed in whole or part.               #
# eDirectory is licensed on a per-domain basis.                      #
#                                                                    #
# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
#                                                                    #
# http://www.edirectory.com | http://www.edirectory.com/license.html #
######################################################################
\*==================================================================*/

# ----------------------------------------------------------------------------------------------------
# * FILE: /includes/views/view_classified_detail_code.php
# ----------------------------------------------------------------------------------------------------

?>

    <div class="responsive-detail">

        <div class="inverse-row">
            <? //include(system_getFrontendPath("detail_maps.php", "frontend", false, CLASSIFIED_EDIRECTORY_ROOT)); ?>
        </div>

        <div class="row-fluid">

            <div class="row-fluid top-info">

                <div class="span10">
                    <h3><?=$classified_title;?></h3>
                </div>

                <div class="span2 share-middle text-right">
                    <?=$classified_icon_navbar?>
                </div>

            </div>
            
            <? if ($classified_category_tree) { ?>
            
            <div class="row-fluid top-info">
                <?=$classified_category_tree?>
            </div>
            
            <? } ?>

        </div>

        <div class="row-fluid">

            <div class="span8">
                <? if ($classified_price != "NULL" && $classified_price != "" || $classified_contactName ) { ?>
                <div class="row-fluid dialog-list">
                        <? if ($classified_price != "NULL" && $classified_price != "") { ?>
                        
                        <dl class="dl-horizontal span6">
                            <dt><?=system_showText(LANG_LABEL_PRICE);?></dt>
                            <dd><?=CURRENCY_SYMBOL." ".$classified_price;?></dd>
                        </dl>
                        
                        <? } ?>
                        
                        <? if ($classified_contactName) { ?>
                        
                        <dl class="dl-horizontal span6">
                            <dt><?=ucfirst(system_showText(LANG_CONTACT))?></dt>
                            <dd><?=nl2br($classified_contactName)?></dd>
                        </dl>
                        
                        <? } ?>  
                </div>
                <? } ?>

                <div class="row-fluid overview mgt-10">
                    
                    <? if ($classified_summary) { ?>
                    
                        <h5><?=system_showText(LANG_LABEL_OVERVIEW);?></h5>

                        <p><?=$classified_summary;?></p>
                    
                    <? } ?>

                    <? if ($classified_description) { ?>
                        <div class="content-box">
                            <h5><?=system_showText(LANG_LABEL_DESCRIPTION);?></h5>
                            <p><?=$classified_description?></p>
                        </div>
                    <? }  if (!empty($classified->video_snippet)) {
        $url = $classified->video_snippet;
        
        parse_str(parse_url($url, PHP_URL_QUERY), $youtube_id);
        ?>
       <center><iframe width="350" height="300" src="http://www.youtube.com/embed/<?= $youtube_id['v']; ?>" frameborder="0" allowfullscreen></iframe>
       </center>
                    <?php } ?>

                </div>

            </div>

            <div class="span4">
  
                <? if (($imageTag && !$classifiedGallery && $onlyMain) || ($tPreview && $imageTag)) { ?>
                    <div class="image">
                        <?=$imageTag?>
                    </div>
                <? } ?>

                <? if ($classifiedGallery) { ?>
                    <div <?=$tPreview ? "class=\"ad-gallery gallery\"" : ""?>>
                        <?=$classifiedGallery?>
                    </div>
                <? } 
                
   
                    if ($classified_email) { ?>
                    
                    <a rel="nofollow" href="<?=$contact_email?>" class="<?=!$tPreview? "fancy_window_tofriend": "";?> btn btn-large btn-success" <?=(!$user ? "style=\"cursor:default;\"" : "");?>><?=system_showText(LANG_SEND_AN_EMAIL);?></a><br />
                    
                <? } ?>
                    
                <? include(system_getFrontendPath("detail_socialbuttons.php", "frontend", false, CLASSIFIED_EDIRECTORY_ROOT)); ?>
                
                <? if (($location) || ($classified_address) || ($classified_address2)) echo "<address>\n";  ?>

                <strong><?=system_showText(LANG_LABEL_ADDRESS)?></strong><br />
                
                <? if ($classified_address) { ?>
                    <span><?=nl2br($classified_address)?></span><br />
                <? } ?>

                <? if ($classified_address2) { ?>
                    <span><?=nl2br($classified_address2)?></span><br />
                <? } ?>

                <? if ($location) { ?>
                    <span><?=$location?></span>
                <? } ?>

                <? if (($location) || ($classified_address) || ($classified_address2)) echo "</address>\n";  ?>

                <? if ($classified_phone) { ?>
                    
                    <hr><strong><?=system_showText(LANG_LABEL_PHONE)?></strong>
                    <br /><?=nl2br($classified_phone)?>
                    
                <? } ?>

                <? if ($classified_fax) { ?>
                    
                    <hr><strong><?=system_showText(LANG_LABEL_FAX)?></strong>
                    <br /><?=nl2br($classified_fax)?>
                    
                <? } ?>

                <? if ($classified_url) { ?>
                    
                    <? if ($user) { ?>
                    
                        <hr><strong><?=system_showText(LANG_LABEL_URL)?>:</strong>
                        <br /><a href="<?=nl2br($classified_url)?>" target="_blank"><?=nl2br($classified_url)?></a>
                        <hr>
                        
                    <? } else { ?>
                        
                        <hr><strong><?=system_showText(LANG_LABEL_URL)?>:</strong>
                        <br /><a href="javascript:void(0);" style="cursor:default"><?=nl2br($classified_url)?></a>
                        <hr>
                        
                    <? } ?>
                    
                <? } ?>
            </div>
            
        </div>
        
    </div>