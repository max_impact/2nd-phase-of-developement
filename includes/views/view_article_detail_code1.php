<?
/* ==================================================================*\
  ######################################################################
  #                                                                    #
  # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
  #                                                                    #
  # This file may not be redistributed in whole or part.               #
  # eDirectory is licensed on a per-domain basis.                      #
  #                                                                    #
  # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
  #                                                                    #
  # http://www.edirectory.com | http://www.edirectory.com/license.html #
  ######################################################################
  \*================================================================== */

# ----------------------------------------------------------------------------------------------------
# * FILE: /includes/views/view_article_detail_code.php
# ----------------------------------------------------------------------------------------------------






include(INCLUDES_DIR . "/views/view_detail_tabs.php");

include_once(EDIRECTORY_ROOT . "/classes/facebook/base_facebook.php");
include_once(EDIRECTORY_ROOT . "/classes/facebook/facebook.php");
if (SELECTED_DOMAIN_ID == 3) {
    $temp1 = 0;
} else {
    $temp1 = 1;
}
?>


<div class="tab-container" style="border-bottom: 1px solid rgb(204, 204, 204); margin-bottom:<?= (SELECTED_DOMAIN_ID == 3) ? '15px' : '40px'; ?>">
    <div id="content_overview" class="tab-content">
        <div class="row-fluid">
            <div class="top-info">
                <h3><?= $article_title ?></h3>
                <div class="row-fluid">
                    <div class="span8">
                        <? if ($summary_review) { ?>
                            <?= $summary_review; ?>
                        <? } ?>
                    </div>
                    <div class="span4 <?= ($summary_review ? "text-right" : "text-left") ?>">
                        <?= $article_icon_navbar ?>
                    </div>

                </div>

            </div>

        </div>
        <div class="row-fluid">

            <? if ($article_category_tree) { ?>
                <div class="span12 top-info">
                    <?= $article_category_tree ?>
                </div>
            <? } ?>

        </div>

        <?php
        $sql = 'Select id from Listing where (level="10" || level="30" || level="50") and status="A" ';
        $art_id = $article->getNumber("id");
        $cat = $article->getNumber("lisiting_cate");
        $lisiting_subcate = $article->getNumber("lisiting_subcate");
        if (SELECTED_DOMAIN_ID == 3) {
            $NonProfitcat = $article->getNumber("lisiting_non_profit_cate");
            $NonProfitlisiting_subcate = $article->getNumber("lisiting_non_profit_subcate");
            $nonprofit_state = $article->getNumber("nonprofit_state");
            $cause_chapter_id = $article->getNumber("cause_chapter_id");

            $NonProfitcat_title = '';
            if ($NonProfitcat != 0 || !empty($NonProfitcat)) {
                //echo "select Listing_Category.listing_id from Listing_Category RIGHT JOIN Listing ON Listing.id=Listing_Category.listing_id WHERE `category_root_id`=".$cat." group by Listing_Category.listing_id";
                $data = mysql_query("select NonProfitListing_Category.listing_id from NonProfitListing_Category RIGHT JOIN Listing ON Listing.id=NonProfitListing_Category.listing_id WHERE `category_root_id`=" . $NonProfitcat . " group by NonProfitListing_Category.listing_id");

                if (mysql_num_rows($data) > 0) {
                    $temp1 = 1;
                    if ($cat == 0) {
                        $temp = 1;
                    }
                    $row = mysql_fetch_assoc($data);
                    $sql.=" and id in (" . $row['listing_id'];
                    while ($row = mysql_fetch_assoc($data)) {
                        $sql.="," . $row['listing_id'];
                    }
                    $sql.=" )";
                }
            }
            if ($NonProfitlisiting_subcate != 0 || !empty($NonProfitlisiting_subcate)) {
                //echo "select Listing_Category.listing_id from Listing_Category RIGHT JOIN Listing ON Listing.id=Listing_Category.listing_id WHERE `category_root_id`=".$cat." group by Listing_Category.listing_id";
                $data = mysql_query("select NonProfitListing_Category.listing_id from NonProfitListing_Category RIGHT JOIN Listing ON Listing.id=NonProfitListing_Category.listing_id WHERE `category_root1_id`=" . $NonProfitlisiting_subcate . " group by NonProfitListing_Category.listing_id");

                if (mysql_num_rows($data) > 0) {
                    $temp1 = 1;
                    if ($cat == 0) {
                        $temp = 1;
                    }
                    $row = mysql_fetch_assoc($data);
                    $sql.=" and id in (" . $row['listing_id'];
                    while ($row = mysql_fetch_assoc($data)) {
                        $sql.="," . $row['listing_id'];
                    }
                    $sql.=" )";
                }
                $data_nonprofit = mysql_query("SELECT title  FROM  NonProfitListingCategory where id = (" . $NonProfitlisiting_subcate . ")");

                $row = mysql_fetch_assoc($data_nonprofit);

                $NonProfitcat_title = $row['title'];
            }
            if ($nonprofit_state != 0 || !empty($nonprofit_state)) {
                //echo "select Listing_Category.listing_id from Listing_Category RIGHT JOIN Listing ON Listing.id=Listing_Category.listing_id WHERE `category_root_id`=".$cat." group by Listing_Category.listing_id";
                $data = mysql_query("select NonProfitListing_Category.listing_id from NonProfitListing_Category RIGHT JOIN Listing ON Listing.id=NonProfitListing_Category.listing_id WHERE `category_root2_id`=" . $nonprofit_state . " group by NonProfitListing_Category.listing_id");

                if (mysql_num_rows($data) > 0) {
                    $temp1 = 1;
                    if ($cat == 0) {
                        $temp = 1;
                    }
                    $row = mysql_fetch_assoc($data);
                    $sql.=" and id in (" . $row['listing_id'];
                    while ($row = mysql_fetch_assoc($data)) {
                        $sql.="," . $row['listing_id'];
                    }
                    $sql.=" )";
                }
            }

            if ($cause_chapter_id != 0 || !empty($cause_chapter_id)) {
                $data_nonprofit_subcategory = mysql_query("SELECT title  FROM  NonProfitListingCategory where id = (" . $cause_chapter_id . ")");

                $row = mysql_fetch_assoc($data_nonprofit_subcategory);

                $NonProfitsubcat_title = $row['title'];

                $data = mysql_query("select NonProfitListing_Category.listing_id from NonProfitListing_Category RIGHT JOIN Listing ON Listing.id=NonProfitListing_Category.listing_id WHERE `category_id`=" . $cause_chapter_id . " group by NonProfitListing_Category.listing_id");
                if (mysql_num_rows($data) > 0) {
                    $temp1 = 1;
                    $row = mysql_fetch_assoc($data);
                    $sql.=" and id in (" . $row['listing_id'];
                    while ($row = mysql_fetch_assoc($data)) {
                        $sql.="," . $row['listing_id'];
                    }
                    $sql.=" )";
                } else {
                    $temp1 = 0;
                }
            }
        }
        $location_1 = $article->getNumber("location_1");
        $location_3 = $article->getNumber("location_3");
        $location_4 = $article->getNumber("location_4");
        $miles = $article->getNumber("miles");
        $where = array();
        if ($cat != 0 || !empty($cat)) {
            //echo "select Listing_Category.listing_id from Listing_Category RIGHT JOIN Listing ON Listing.id=Listing_Category.listing_id WHERE `category_root_id`=".$cat." group by Listing_Category.listing_id";
            $data = mysql_query("select Listing_Category.listing_id from Listing_Category RIGHT JOIN Listing ON Listing.id=Listing_Category.listing_id WHERE `category_root_id`=" . $cat . " group by Listing_Category.listing_id");

            if (mysql_num_rows($data) > 0) {
                $temp = 1;
                if (SELECTED_DOMAIN_ID == 3) {
                    if ($NonProfitcat == 0) {
                        $temp1 = 1;
                    }
                }
                $row = mysql_fetch_assoc($data);
                $sql.=" and id in (" . $row['listing_id'];
                while ($row = mysql_fetch_assoc($data)) {
                    $sql.="," . $row['listing_id'];
                }
                $sql.=" )";
            }
        }

        if ($lisiting_subcate != 0 || !empty($lisiting_subcate)) {

            $data = mysql_query("select Listing_Category.listing_id from Listing_Category RIGHT JOIN Listing ON Listing.id=Listing_Category.listing_id WHERE `category_id`=" . $lisiting_subcate . " group by Listing_Category.listing_id");
            if (mysql_num_rows($data) > 0) {
                $temp = 1;
                $row = mysql_fetch_assoc($data);
                $sql.=" and id in (" . $row['listing_id'];
                while ($row = mysql_fetch_assoc($data)) {
                    $sql.="," . $row['listing_id'];
                }
                $sql.=" )";
            } else {
                $temp = 0;
            }
        }

        if ($location_1 != 0 || !empty($location_1)) {
            $sql.=" and location_1=" . $location_1;
        }
        if ($location_3 != 0 || !empty($location_3)) {
            $sql.=" and location_3=" . $location_3;
            $dataa = "select abbreviation from " . _DIRECTORYDB_NAME . ".Location_3 where id=" . $location_3 . "";
            $st = @mysql_result(mysql_query($dataa), 0);
        }
        if ($location_4 != 0 || !empty($location_4)) {
            //$sql.=" and location_4=" . $location_4;
            $ctda = "select name from " . _DIRECTORYDB_NAME . ".Location_4  where id=" . $location_4 . "";
            $ct = @mysql_result(mysql_query($ctda), 0);
        }
        if ($miles != '' && $miles != 0) {
            $row['Zipcode'] = '14445';
            if (($location_3 != 0 || !empty($location_3)) && ($location_4 != 0 || !empty($location_4))) {

                $sqll = "SELECT Zipcode FROM `zip` WHERE State = '" . $st . "' AND City = '" . $ct . "' order by Zipcode desc";
                $resultzip = mysql_query($sqll);
                if ($resultzip) {
                    $row = mysql_fetch_assoc($resultzip);
                }
            }

            zipproximity_getWhereZipCodeProximity($row['Zipcode'], $miles, $whereZipCodeProximity, $order_by_zipcode_score);
            $sql.=" and " . $whereZipCodeProximity;
        }
        if ($location_1 != 0 || !empty($location_1)) {
            $name = mysql_query("select `name`,`abbreviation` from " . _DIRECTORYDB_NAME . ".Location_1 where id=" . $location_1);

            $country = mysql_fetch_assoc($name);
        }
        if ($location_3 != 0 || !empty($location_3)) {
            $name = mysql_query("select `name`,`abbreviation` from " . _DIRECTORYDB_NAME . ".Location_3 where id=" . $location_3);

            $state = mysql_fetch_assoc($name);
        }

        if ($location_4 != 0 || !empty($location_4)) {
            $name = mysql_query("select `name`  from " . _DIRECTORYDB_NAME . ".Location_4 where id=" . $location_4);

            $city = mysql_fetch_assoc($name);
            $cityart = $city['name'];
        }
        if (SELECTED_DOMAIN_ID != 3) {
            ?>                <div class="socal_overflow" style="height:auto;width:355px">                       <div style="padding-bottom: 15px;">    
                    <h1 style="font-size:20px"><?php echo $city['name'] . ',&nbsp;' . $state['name'] . '&nbsp;' . $country['name'] ?> </h1>                    </div>                                    </div> </div>

        <?php
    }
    if ($temp == 1 && $temp1 == 1 && SELECTED_DOMAIN_ID != 3) {
        $listing = mysql_query($sql);
        if (mysql_num_rows($listing) > 0) {
            $x = array();
            while ($listing_rows = mysql_fetch_assoc($listing)) {
                $x[] = $listing_rows['id'];
            }
            $listing_id = implode(',', $x);
            if (SELECTED_DOMAIN_ID == 1) {
//                $sql = "SELECT Listing_Summary.*  FROM Listing_Summary where id in (" . $listing_id . ") ORDER BY fbcount DESC";
//                $results = $dbObj->query($sql);
//                while ($result = mysql_fetch_assoc($results)) {
//                    $url = "http://" . $_SERVER['HTTP_HOST'] . "/listing/" . $result['friendly_url'] . ".html";
//
//                    include(EDIRECTORY_ROOT . "/fb_share.php");
//
//                    $data2 = mysql_query("UPDATE Listing_Summary  SET fbcount=(" . $fbcount . ")  where friendly_url = ('" . $result['friendly_url'] . "')");
////               
//                }
                $sql1 = "SELECT Listing_Summary.*  FROM Listing_Summary where id in (" . $listing_id . ") ORDER BY fbcount DESC";
            } elseif (SELECTED_DOMAIN_ID == 2) {
                if ($miles != '' && $miles != 0) {
                    zipproximity_getWhereZipCodeProximity($row['Zipcode'], $miles, $whereZipCodeProximity, $order_by_zipcode_score);
                    $output = str_replace("AS zipcode_score", "", $order_by_zipcode_score);

                    $sql1 = "SELECT Listing_Summary.*  FROM Listing_Summary where id in (" . $listing_id . ") ORDER BY $output";
                }
            } else {
                $sql1 = "SELECT Listing_Summary.*  FROM Listing_Summary where id in (" . $listing_id . ") ";
            }
            //echo $sql1;exit;
            $resultzip = $dbObj->query($sql1);
            $j = 0;
            $count = 1;
            ?>

            <?php
            if ($resultzip) {
                $alph = range("A", "Z");
                $j = 0;
                $LocationData = array();
                while ($list_det = mysql_fetch_assoc($resultzip)) {
                    $LocationData[] = array($list_det['longitude'], $list_det['latitude'], $list_det['address'], $list_det['title']);
                    if ($list_det['location_1'] != 0 || !empty($list_det['location_1'])) {
                        $name = mysql_query("select `name`,`abbreviation` from " . _DIRECTORYDB_NAME . ".Location_1 where id=" . $list_det['location_1']);
                        if ($name)
                            $country = mysql_fetch_assoc($name);
                    } if ($list_det['location_3'] != 0 || !empty($list_det['location_3'])) {
                        $name = mysql_query("select `name`,`abbreviation` from " . _DIRECTORYDB_NAME . ".Location_3 where id=" . $list_det['location_3']);
                        if ($name)
                            $state = mysql_fetch_assoc($name);
                    }
                    if ($list_det['location_4'] != 0 || !empty($list_det['location_4'])) {
                        $name = mysql_query("select `name`  from " . _DIRECTORYDB_NAME . ".Location_4 where id=" . $list_det['location_4']);
                        if ($name)
                            $city = mysql_fetch_assoc($name);
                    }
                    $data = mysql_query("SELECT category_root_id  FROM Listing_Category where listing_id = (" . $list_det['id'] . ")");
                    $row = mysql_fetch_assoc($data);
                    $root_id = $row['category_root_id'];
                    $data2 = mysql_query("SELECT title  FROM  ListingCategory where root_id = (" . $root_id . ")");
                    $row = mysql_fetch_assoc($data2);
                    $cat_title = $row['title'];
                    $data = mysql_query("SELECT category_root_id  FROM Listing_Category where listing_id = (" . $list_det['id'] . ")");
                    $row = mysql_fetch_assoc($data);
                    $root_id = $row['category_root_id'];
                    ?>
                    <div class="new_box" >
                        <span style="display: block; margin-right: -35px; padding: 1px 6px; width: 9px; height: 32px; background-image: url('http://<?php echo $_SERVER[HTTP_HOST] ?>/images/chart.png');"><?php echo $alph[$j]; ?></span>
                        <div class="new_head_box" style="font-size: 12px; float: left; margin-top: -28px; margin-left: 28px;" >
                            <div class="new_head2">
                                <a style="font-size:12px;text-decoration:underline;" href="http://<?= $_SERVER['HTTP_HOST'] ?>/listing/<?php echo $list_det['friendly_url'] ?>.html"><?= system_showTruncatedText($list_det['title'], 25); ?></a> 
                                <?= ($lisiting_subcate_det["title"]) ? '(' . $lisiting_subcate_det["title"] . ')' : ''; ?>					                        </div>
                                <?php echo $list_det['address']; ?> , <?php echo ($city['name']) ? $city['name'] . ',' : '' ?><?= $state['abbreviation']; ?> 
                            <br/>
                            <div class="new_site_link"><a href="tel:<?php echo $list_det['phone']; ?>"><?php echo $list_det['phone']; ?> </a>&nbsp;&nbsp;&nbsp; <a href="<?php echo $list_det['url'] ?>"><?php echo parse_url($list_det['url'], PHP_URL_HOST); ?></a>&nbsp;&nbsp;&nbsp;
                                <a onclick="javascript:window.open('http://maps.google.com/maps?q=<?php echo $list_det["latitude"] ?>,<?php echo $list_det["longitude"] ?>', 'popup', '')" href="javascript:void(0);">Get driving directions</a>

                            </div>
                        </div>
                        <div class="clear"></div>
                        <p style=" font-size: 11px;  line-height: 18px;margin-left:28px">
                            <?= system_showTruncatedText($list_det['description'], 100); ?>
                        </p>
                        <?php //                    if(SELECTED_DOMAIN_ID==3){  ?>
                <!--<div style="font-size: 15px; margin-bottom: 20px;color:#b55cfc"> Support: <span style="text-transform: uppercase;color:#7C05DC"><?php echo $NonProfitcat_title ?>.</span></div>-->

                        <?php
                        //   }
                        if (SELECTED_DOMAIN_ID == 1) {
//                    echo $list_det['fbcount'];
                            $title = $list_det['title'];
                            $frndlyurl = $list_det['friendly_url'];
                            $url = "http://" . $_SERVER['HTTP_HOST'] . "/listing/" . $list_det['friendly_url'] . ".html";
                            $summary = $list_det['description'];
//                            include(EDIRECTORY_ROOT . "/fb_share.php");
                            //////////////////////////
                            $frndurl = $list_det['friendly_url'];

                            setting_get("commenting_fb", $commenting_fb);
                            setting_get("commenting_fb_number_comments", $commenting_fb_number_comments);
							
                            ?>
                            <div class="fcontainer">
                                <div class="comment"></div>
                                <a href="#dialogdiv<?php echo $list_det['id'] ?>" id="btniframe" class="iframe testhover button" style="margin-left:29px; padding-right: 30px; padding-bottom: 11px;"><span>Comments</span></a>
                                <div id="votediv" style="display:inline-block">
                                    <a href="" onClick="read($(this),<?php echo $list_det['id'] ?>);
                                                            return false;" class="button" style="background-color:#A6D215 !important;margin-left:29px"><img src="<?= DEFAULT_URL . '/theme/' . EDIR_THEME . '/images/imagery/Megaphone.png' ?>"/><span>Vote For Us</span></a>

                                    <div class="countParent" style="display:inline-block; position: relative; top: -11px; left: -11px;">
                                        <img width="10" style="position: relative;
                                             left: 7px; top:-1px;" src="<?= DEFAULT_URL . "/theme/" . EDIR_THEME . "/images/imagery/tip.png" ?>"/>

                                        <div class="buble"><span class="facebaook_count"><?php echo $list_det['fbcount']; ?></span></div>
                                    </div>    
                                </div>
                                <!-- Each listing comments  -->
                                <div class="dialog dialogdiv" id="dialogdiv<?php echo $list_det['id'] ?>" style="display:none" title="Basic dialog">
                                    <div class="fb-comments" data-href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/listing/share/<?php echo $list_det['friendly_url'] ?>.html" data-numposts="5" data-colorscheme="light"></div>
                                </div>
                                <!--       -->

                                <script>
                                    function read(x, y)
                                    {
                                    var div = x.parents(".new_box");
                                            var url = div.find(".new_head2 a").attr('href');
                                            var title = div.find(".new_head2 a").text();
                                            var count = div.find(".buble span").text();
                                            var friendurl = url.split("/");
                                            var detailLink = friendurl[4];
                                            var facebaook_count = x.siblings(".countParent").find(".facebaook_count").text();
                                            var DEFAULT_DB = "<?php echo DEFAULT_DB ?>";
                                            var SELECTED_DOMAIN_ID = "<?php echo SELECTED_DOMAIN_ID ?>";
                                            
                                            FB.init({
                                    appId: '1412617082285011',
                                            status: true,
                                            cookie: false,
                                            xfbml: true,
                                            oauth: true
                                    });
                                            FB.getLoginStatus(function(response) {
											alert(response);
											
                                    if (response.status == 'connected') {
                                    var uid = response.authResponse.userID;
									console.log(response);
									
                                            accToken = response.authResponse.accessToken;
                                            FB.api('https://graph.facebook.com/' + uid + '/edirectorytoplocal:vote?access_token=' + accToken, 'post', {listing:url + '&fbrefresh=1' });
								              }
								 
                                 if (response.authResponse) {
								 $.get("../frontend/view_fb_count.php", {
                                    async: false,
                                            facebaook_count: facebaook_count,
                                            friendly_url: detailLink,
                                            DEFAULT_DB: DEFAULT_DB,
                                            SELECTED_DOMAIN_ID: SELECTED_DOMAIN_ID


                                    }, function(data) {
                                    if (data == 1) {
                                    newfb = parseInt(facebaook_count, 10) + parseInt('1', 10);
                                            x.siblings(".countParent").find(".facebaook_count").text(newfb);
                                    }

                                 });
											}
											 else {
                                   FB.login(function(response) {
                                 if (response.authResponse) {
								 $.get("../frontend/view_fb_count.php", {
                                    async: false,
                                            facebaook_count: facebaook_count,
                                            friendly_url: detailLink,
                                            DEFAULT_DB: DEFAULT_DB,
                                            SELECTED_DOMAIN_ID: SELECTED_DOMAIN_ID


                                    }, function(data) {
                                    if (data == 1) {
                                    newfb = parseInt(facebaook_count, 10) + parseInt('1', 10);
                                            x.siblings(".countParent").find(".facebaook_count").text(newfb);
                                    }

                                 });
								 }});
                                    }
                                  });
								  
								  
                                  }
                    ////////////////////////////////////////////////////////



                                            $(".fcontainer a.testhover").on("click", function(e) {

                                    var x = $(this);
                                            var div = x.parents(".new_box");
                                            var url = div.find(".new_head2 a").attr('href');
                                            var title = div.find(".new_head2 a").text();
                                            var count = div.find(".buble span").text();
                                            var friendurl = url.split("/");
                                            var detailLink = friendurl[4];
                                            //                                           div.find(".fb-comments").find(".composer .clearfix .mainCommentForm").css("display", '');



                                            $(".fcontainer a.testhover").fancybox({'width': 1000,
                                            'height': 1000,
                                    });
                                            //////////

                                            e.preventDefault();
                                    });                               
                                                                  
                                </script>





                                <style>
                                    .button {
                                        display: inline-block;
                                        zoom: 1; /* zoom and *display = ie7 hack for display:inline-block */
                                        *display: inline;
                                        vertical-align: baseline;
                                        margin: 0 2px;
                                        outline: none;
                                        cursor: pointer;
                                        text-align: center;
                                        text-decoration: none;
                                        font: 12px/100% Arial, Helvetica, sans-serif;
                                        padding: .7em 1em .75em 1.5em;

                                        -webkit-border-radius: .3em; 
                                        -moz-border-radius: .3em;
                                        border-radius: .3em;
                                        border-width:1px;
                                        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
                                        -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
                                        box-shadow: 0 1px 2px rgba(0,0,0,.2);
                                        color:#fff !important;
                                        background-color:#506271;
                                        font-weight:bold;
                                    }
                                    .button:hover {
                                        text-decoration: none;
                                    }
                                    .button:active {
                                        position: relative;
                                        top: 1px;
                                    }
                                    .button img{width: 16px; float:left; border-width:0px;}
                                    .button span{float:right;margin-left: 10px; margin-top: 4px;}
                                    .buble{display:inline-block; border:1px solid #B9B8B8; -webkit-border-radius: .3em; -moz-border-radius: .3em;
                                           border-radius: .3em; padding:0.5em 0.6em 0.35em; }
                                    </style>
                                </div>
                            <?php } ?>					</div>
                        <script>
                                    //    function vote_button(x) {
                                    ////           alert(x.parents(".new_box").html());
                                    //    
                                    //     var div=x.parents(".new_box");
                                    //    var url=div.find(".new_head2 a").attr('href');
                                    //    var title=div.find(".new_head2 a").text();
                                    //    var count=div.find(".buble span").text();
                                    // 
                                    //        $(".tab-container").hide();
                                    //        $(".span4").hide();
                                    //        $(".row-fluid.middle-info.overview").hide();
                                    //        $(".tabs.nav.nav-tabs").hide();
                                    //        $('#Votenow div h3').text(title);
                                    //        $('#Votenow').find('.VotenowParnet').find('span').text(count);
                                    //        if($("#VotenowParnet input").length >1){
                                    //        $('#VotenowParnet').remove('input');  
                                    //        } 
                                    //        $("#VotenowParnet").append('<input type="hidden" name="url" id="url" value="'+url+'" /><input type="hidden" name="title" id="title" value="'+title+'" />')
                                    //        $("#Votenow").show();
                                    //
                                    //    }

                                    /* function fbcall(x) {
                                                     
                                     var div = x.parents(".new_box");
                                     var url = div.find(".new_head2 a").attr('href');
                                     var title = div.find(".new_head2 a").text();
                                     var count = div.find(".buble span").text();
                                     var friendurl = url.split("/");
                                     var detailLink = friendurl[4];
                                                     
                                     var facebaook_count = x.siblings(".countParent").find(".facebaook_count").text();
                                     var DEFAULT_DB = "<?php echo DEFAULT_DB ?>";
                                     var SELECTED_DOMAIN_ID = "<?php echo SELECTED_DOMAIN_ID ?>";
                                                     
                                     $.get("../frontend/view_fb_count.php", {
                                     async: false,
                                     facebaook_count: facebaook_count,
                                     friendly_url: detailLink,
                                     DEFAULT_DB: DEFAULT_DB,
                                     SELECTED_DOMAIN_ID: SELECTED_DOMAIN_ID
                                                     
                                                     
                                     }, function(data) {
                                     if (data == 1) {
                                     newfb = parseInt(facebaook_count, 10) + parseInt('1', 10);
                                     x.siblings(".countParent").find(".facebaook_count").text(newfb);
                                     }
                                                     
                                     });
                                                     
                                                     
                                     window.fbAsyncInit = function() {
                                     FB.init({
                                     appId: '1412617082285011',
                                     status: true,
                                     cookie: false,
                                     xfbml: true,
                                     oauth: true
                                     });
                                     FB.ui({method: 'feed', name: title, display: 'popup', link: url, description: "<?php !empty($summary) ? $summary : '' ?>"},
                                     function(response) {
                                     console.log(response);
                                     if (response && response.post_id) {
                                     window.location = url;
                                                     
                                                     
                                                     
                                     // $('#cmmentnow span h3').text("Recommend "+title);
                                     // $(".row-fluid.tablet-full").find(".span8").css("width", "100%");
                                     //
                                     //       var Link = "<?php echo LISTING_DEFAULT_URL . "/" . ALIAS_SHARE_URL_DIVISOR ?>/" +detailLink;
                                     //    
                                     //
                                     //// $('#cmmentnow .fb-comments ').attr('data-href',Link);
                                     //                    $("#Votenow").hide();
                                     //                    $(".span4").hide();
                                     //                    $("#cmmentnow").show();
                                     //                    
                                     //                    
                                     //                    document.getElementById('commentlink').innerHTML='<div class="fb-comments" data-href="'+Link+'" data-num-posts="<?= $commenting_fb_number_comments ?>" data-width="500"></div>';
                                     //                
                                     //    FB.XFBML.parse();
                                     //     $("#commentlink").find("span").css("width", "500px");
                                     //                    $("#commentlink").find("iframe").css("width", "500px");
                                     //                   
                                     } else {
                                     console.log('Post was not published.');
                                                     
                                     }
                                     }
                                     );
                                                     
                                                     
                                     }
                                     (function(d) {
                                     var js, id = 'facebook-jssdk';
                                     if (d.getElementById(id)) {
                                     return;
                                     }
                                     js = d.createElement('script');
                                     js.id = id;
                                     js.async = true;
                                     js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                     d.getElementsByTagName('head')[0].appendChild(js);
                                     }(document));
                                     } */
                        </script>
                        <br/>
                        <?php
                        $j++;
                        $data = mysql_query("SELECT *  FROM Promotion where listing_id = (" . $list_det['id'] . ")");

                        if ($data) {
                            while ($row = mysql_fetch_assoc($data)) {

                                $imageObj = new Image($row['image_id']);
                                $imageTag = "";

                                if ($imageObj->imageExists()) {
//                           print_r($row['name']);exit;
                                    $imageTag .= "<div class=\"no-link\" " . (RESIZE_IMAGES_UPGRADE == "off" ? "style=\"text-align:center\"" : "") . ">";
                                    $imageTag .= $imageObj->getTag(THEME_RESIZE_IMAGE, IMAGE_PROMOTION_FULL_WIDTH, IMAGE_PROMOTION_FULL_HEIGHT, $row['name'], THEME_RESIZE_IMAGE);
                                    $imageTag .= "</div>";
                                    $auxImgPath = $imageObj->getPath();
                                } else {
                                    $imageTag .= "<span class=\"no-image no-link\"></span>";
                                }


                                $dealname = $row['name'];
                                $promotionurl = "http://" . $_SERVER['HTTP_HOST'] . "/deal/" . $row["friendly_url"] . ".html";
//               print_r($list_det['friendly_url']);exit;
                                $deal_price = string_substr($row["dealvalue"], 0, (string_strpos($row["dealvalue"], ".")));
                                $deal_cents = string_substr($row["dealvalue"], (string_strpos($row["dealvalue"], ".")), 3);
                                $listing_link = "http://" . $_SERVER['HTTP_HOST'] . "/listing/" . $list_det['friendly_url'] . ".html";

                                if (SELECTED_DOMAIN_ID == 2) {
                                    ?>
                                    <div class="summary summary-deal summary-small" style=" background-color: #FFFFFF;
                                    border: 1px solid #DEDEDE;width: 514px; margin-bottom: 21px;margin-top: 21px">
                                    <section>

                                        <div class="row-fluid title" style=" border-bottom: 1px solid #DEDEDE;">

                                            <div class="span8" style="padding-left:12px">
                                                <h3><p style="font-size:13px">
                                                        <a href="<?= $promotionurl ?>" style="cursor:default;font-size:9pt;line-height: 22px" title="<?= $row['name'] ?>"><?= $row['name'] ?></a>
                                                    </p> 
                                                </h3>
                                                <? //if ($listingTitle) {     ?>
                                                <p><?= system_showText(LANG_BY) ?> <a href="<?= $promotionurl ?>" title="<?= $list_det['title'] ?>"><?= $list_det['title'] ?></a></p>
                                                <? //}   ?>
                                            </div>

                                            <? if ($sold_out) { ?>

                                                <div class="deal-tag-small" style="">
                                                    <div class="name-tag-deal soldout"><?= system_showText(DEAL_SOLDOUT); ?></div>
                                                </div>

                                            <? } else { ?>

                                                <div class="deal-tag-small">
                                                    <a href=<?= $promotionurl ?> class="name-tag-deal" style="margin-top:-39px;margin-left:403px;text-align:center;display:block;width: 114px;padding-top: 7px;height:29px; float: left;background-color:#1B418F ;color:white">
                                                        <?= (is_numeric($deal_price) && $deal_price > 0 ? CURRENCY_SYMBOL . $deal_price . ($deal_cents ? "<span class=\"cents\">" . $deal_cents . "</span>" : "") : system_showText(LANG_FREE)); ?>                            
                                                        <b class="divisor"></b>
                                                        <?= $offer . " " . system_showText(LANG_DEAL_OFF); ?>
                                                    </a>
                                                    <div style="height: 0px;margin-left:403px;float:left;margin-top: -40px ; width: 0px;top: 0px; border-top: 19px solid transparent; border-bottom: 19px solid transparent; left: -3px; border-left: 14px solid rgb(255, 255, 255);"> </div>
                                                </div>

                                            <? } ?>

                                        </div>

                                        <div class="media-body">

                                            <div class="row-fluid info">

                                                <div class="span4">
                                                    <div class="summary-image">
                                                        <?= $imageTag; ?>
                                                    </div>
                                                </div>

                                                <div class="span8">
                                                    <? if ($row["description"]) { ?>
                                                        <p><?= $row["description"]; ?></p>
                                                    <? } ?>
                                                </div>

                                            </div>

                                        </div>

                                    </section>

                                    <div class="row-fluid  line-footer" style="  background-color: #F5F5F4;
                                         color: #5E5E5E; max-height: 30px;
                                         padding: 0 2%;
                                         width: 96%;">

                                        <div class="span12 review">
                                            <div class="rate"><div class="rate-stars">
                                                    <div class="stars-rating color-6">
                                                        <div class="rate-0"></div>
                                                    </div>                                                      
                                                </div><p><a class="fancy_window_iframe" href="http://<?php echo $_SERVER[HTTP_HOST] ?>/popup/popup.php?pop_type=reviewformpopup&item_type=promotion&item_id=<?php echo $row['id'] ?>" rel="nofollow">Be the first to review this item!</a></p></div>                    
                                        </div>

                                    </div>



                                </div>

                                <?php
                            }
                        }
                    }
                    ?>

                    <div style="clear:both"></div>	 
                    <?php
                }
            }
        }
    }
    ?> 



</div>


<div class="row-fluid middle-info overview"> 

    <? if ($imageTag || $articleGallery) { ?>

        <div class="span7">

            <section class="gallery-overview" style="margin-bottom: 6px;">

                <? if (($imageTag && !$articleGallery && $onlyMain) || ($tPreview && $imageTag)) { ?>

                    <div class="image">
                        <?= $imageTag ?>
                    </div>

                <? } ?>

                <? if ($articleGallery) { ?>

                    <div <?= $tPreview ? "class=\"ad-gallery gallery\"" : "" ?>>
                        <?= $articleGallery ?>
                    </div>

                <? } ?>

            </section>
            <?php
            if (!empty($article->video_snippet)) {
                $url = $article->video_snippet;
                parse_str(parse_url($url, PHP_URL_QUERY), $youtube_id);
                ?>
                <iframe width="350" height="300" src="http://www.youtube.com/embed/<?= $youtube_id['v']; ?>" frameborder="0" allowfullscreen></iframe>

            <?php } ?>
        </div>

    <? } ?>

    <? if ($article_publicationDate) { ?>

        <h6><?= system_showText(LANG_ARTICLE_PUBLISHED) ?></h6>
        <?= $article_publicationDate ?>

    <? } ?>

    <? if ($article_author) { ?>

        <?= system_showText(LANG_BY) ?> <?= $article_authorStr ?>

    <? } elseif ($article_name) { ?>

        <?= system_showText(LANG_BY) ?> <?= $article_name ?>

    <? } ?>

    <? if ($article_content) { ?>

        <p class="long"><?= ($article_content) ?></p>

    <? } ?>

</div>



<? if ($detail_review) { ?>

    <div class="flex-box-group color-3 helpful-reviews">

        <h2>
            <i class="icon-pencil"></i>
            <?= system_showText(LANG_LABEL_HELPFUL_REVIEWS); ?>

            <? if (count($reviewsArr) > 3) { ?>
                <a rel="nofollow" class="view-more" href="javascript:void(0);" <?= (!$user ? "style=\"cursor:default;\"" : "onclick=\"loadReviews('article', $article_id, 1); showTabDetail('review', true);\""); ?>><?= str_replace("[x]", count($reviewsArr), system_showText(LANG_LABEL_SHOW_REVIEWS)); ?></a>
            <? } else { ?>
                <a rel="nofollow" class="view-more <?= $class; ?>" href="<?= ($user ? $linkReviewFormPopup : "javascript:void(0);"); ?>"><?= system_showText(LANG_REVIEWRATEIT); ?></a>
            <? } ?>
        </h2>

        <?= $detail_review ?>

    </div>

<? } ?>



<? if ($detail_review) { ?>

    <div id="content_review" class="tab-content" <?= $activeTab == "review" ? "style=\"\"" : "style=\"display: none;\""; ?>>

        <div class="row-fluid">

            <div class="span12  top-info">

                <div class="span8">

                    <h3><?= $article_title ?></h3>

                    <div class="stars-rating">
                        <div class="rate-<?= (is_numeric($rate_avg) ? $rate_avg : "0") ?>"></div>
                    </div>

                </div>

                <div class="span4">
                    <a rel="nofollow" class="btn-review btn btn-success <?= $class; ?>" href="<?= ($user ? $linkReviewFormPopup : "javascript:void(0);"); ?>"><?= system_showText(LANG_REVIEWRATEIT); ?></a>
                </div>

            </div>

        </div>

        <div id="loading_reviews">
            <img src="<?= DEFAULT_URL . "/theme/" . EDIR_THEME . "/images/iconography/icon-loading-location.gif" ?>" alt="<?= system_showText(LANG_WAITLOADING) ?>"/>
        </div>

        <div id="all_reviews" class="row-fluid"></div>

    </div>
    <!--</div>-->
<? } ?>

<?php
$data = mysql_query("SELECT cat_1_id FROM Article where id = (" . $art_id . ")");

$row = mysql_fetch_assoc($data);
$c1_id = $row['cat_1_id'];
if ($c1_id) {

    $data2 = mysql_query("SELECT title  FROM ArticleCategory where id =" . $c1_id . " and category_id=0");
    if (mysql_num_rows($data2) > 0) {
        $row = mysql_fetch_assoc($data2);
        $art_cat_title = $row['title'];
    } else {
        $data2 = mysql_query("SELECT title  FROM ArticleCategory where id = (SELECT category_id  FROM ArticleCategory where id =(" . $c1_id . ") )");
        $row = mysql_fetch_assoc($data2);
        $art_cat_title = $row['title'];
    }
}
$datasql = mysql_query("SELECT title  FROM  ListingCategory where id  =(" . $lisiting_subcate . ") ");
$row = mysql_fetch_assoc($datasql);
$article_sub_cat = $row['title'];



if (SELECTED_DOMAIN_ID == 1 || SELECTED_DOMAIN_ID == 3) {
    ?>
    <script>
                //        $(document).ready(function(){
                //            alert("eeeee")
                //        });
    <?php
    if (!$signUpItem) {
        if (SELECTED_DOMAIN_ID == 1) {
            ?>
                $(".header-brand ").append('<div class="banner" style="clear: both; text-align: center;"><div style="font-size: 50px; margin-bottom: 20px;">TOP LOCAL RATED</div><div style="text-transform: uppercase; font-size: 17px; text-align: center;"><span style="color: red;"><?php echo $article_sub_cat ?></span> in <span style="color: red;"><?php echo $cityart ?>, <?php echo $state['name'] ?></span></div><div style="clear: both; margin-top: 20px; overflow: hidden;"><p>Find the best <span><?php echo $article_sub_cat ?></span> businesses ranked by customer recommendations servicing the <span><?php echo $cityart ?> </span>,<span> <?php echo $state['name'] ?></span> area here on TopLocalRated.com. Read recommendations and ratings on the best <span><?php echo $article_sub_cat ?> </span> in <span><?php echo $cityart ?> </span>  . Get real customer reviews, phone numbers, maps and more! </p></div> </div> ')
            <?php
        } elseif (SELECTED_DOMAIN_ID == 3) {
            if ($NonProfitsubcat_title) {
                ?>
                    $(".container-fluid").prepend('<div class="banner" style="clear: both; text-align: center;  margin-top: 12px;"><div style="text-transform: uppercase;font-size: 40px; margin-bottom: 20px;color:#ed207b;"><?php echo $NonProfitcat_title ?></div><div style="font-size: 17px; text-align: center;color:#7c05dc"><span style="color: #ed207b;text-transform: uppercase;"><?php echo $NonProfitsubcat_title ?></span> <br/>in <span style="color: #ed207b;text-transform: uppercase;"><?php echo $cityart ?>, <?php echo $state['name'] ?></span></div></div>')

            <?php } else { ?>
                    $(".container-fluid").prepend('<div class="banner" style="clear: both; text-align: center;  margin-top: 12px;"><div style="text-transform: uppercase;font-size: 40px; margin-bottom: 20px;color:#ed207b;"><?php echo $NonProfitcat_title ?></div></div>')

                <?php
            }
        }
    }
    ?>
    </script>

<?php } ?>
<style>

    .VotenowParnet {
        margin-left: 196px;
        width: 320px;
    }
    .VotenowParnet .Votenow {
        background-image: -ms-linear-gradient(top, #B8E229 0%, #93C100 100%);
        background-image: -moz-linear-gradient(top, #B8E229 0%, #93C100 100%);
        background-image: -o-linear-gradient(top, #B8E229 0%, #93C100 100%);
        background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #B8E229), color-stop(1, #93C100));
        background-image: -webkit-linear-gradient(top, #B8E229 0%, #93C100 100%);
        background-image: linear-gradient(to bottom, #B8E229 0%, #93C100 100%);

        border-radius: 5px 5px 5px 5px;
        color: #FFFFFF !important;
        font-size: 22pt;

        height: 39px;
        padding: 10px 30px;
        width: 117px;
    }

</style>



<div id="Votenow" style="display:none;margin-left: 176px; width: 546px;">
    <div style=' clear: both;
         color: #CD3636;font-size: 18pt;text-align: center;line-height: 30px;

         margin-bottom: 31px;'> Help make <h3 style="font-size: 18pt; font-weight: normal;"><?php echo $frndlyurl ?></h3> the Top Rated in <?php echo $city['name'] ?> for <?php echo $cat_title ?></div>
    <div class='VotenowParnet' id="VotenowParnet">

        <a onClick="fbcall();
                return false;" class='Votenow' href="">Vote now!</a>

        <span style="color:#EFB653 ;border: 1px solid #EEEEEE; font-size: 13pt;
              margin-left: 10px;
              padding: 5px 10px;">  </span>
    </div>
</div>

<?php
$moduleURL = urldecode($moduleURL);

if (!$moduleMessage && !$hideDetail && $commenting_fb && ($user || $tPreview)) {
    ?>
    <div id="cmmentnow" style="display:none">	<div class="pnt-0">


            <span style='color: #CA3E3F;font-size: 18pt;line-height: 26px;text-align:center'>
                <h3 style="font-size: 18pt; font-weight: normal;"> </h3> with a great review 
                please don't forget to mention us by name in your comment !
            </span>


            <?
            if ($user) {

                //$detailLink = LISTING_DEFAULT_URL . "/" . ALIAS_SHARE_URL_DIVISOR . "/" . $frndlyurl . ".html";
                ?>
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id))
                        return;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1412617082285011";
                        fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
                window.fbAsyncInit = function() {
        //            FB.Event.subscribe('comment.create',
        //                    function(response) {
        //
        //
        //                        var skip = '<br /><br /><iframe src="http://iconnectedmarketing.net/onlineSignup/embed_osp.php?user=iconnected&dname=406ac8aa9c80be06aebddcc2407a8680" frameborder="0" width="220" height="248" scrolling="no" style="border:#3366FF solid 4px" ></iframe><a href=http://<?php echo $_SERVER[HTTP_HOST] ?>/article style=" margin-top:269px; background: none repeat scroll 0 0 #888888;border-radius: 3px 3px 3px 3px;clear: both;color: #FFFFFF;float: right;font-size: 12pt;padding: 1px 10px;">Skip</a></div>';
        //                        var msg = '<div style="color: #CA3E3F;font-size: 18pt;text-align: center;line-height: 26px; width: 750px;margin: 0 auto;">Thanks for supporting our Top Local Rated Businesses! Sign up for weekly updates of TLR Businesses ad Special Offers'
        //                        $(".tablet-full").html(msg + skip);
        //                    });
        //            FB.Event.subscribe('comment.remove',
        //                    function(response) {
        //
        //
        //                        alert(response);
        //                    });

        };
                //     $(function(){alert("eeeee")})



                </script>



                <? //print_r("tessssst".$user);exit;      ?>
                <div id="commentlink" style="margin-top:17px;text-align: center">
                    <div class="fb-comments" data-href="" data-num-posts="<?= $commenting_fb_number_comments ?>" data-width="<?= FB_COMMENTWIDTH ?>"></div>
                </div>

            <? } else { ?>
                <img src="<?= THEMEFILE_URL . "/" . EDIR_THEME . "/schemes/" . EDIR_SCHEME . "/images/structure/facebook-comment-sample.png" ?>" alt="Sample" />
            <? } ?>

        </div>
    </div>

<? } else { ?>
    <br class="clear" />
    <?
}


////////////////////////////
if (SELECTED_DOMAIN_ID == 3) {
    ?>

    <div class="socal_overflow" style="height: auto; border-top: 1px solid rgb(204, 204, 204); width: 626px;">                       <div style="padding-top: 4px; padding-bottom: 4px;">    
            <h1 style="font-size:20px"><?php echo $city['name'] . ',&nbsp;' . $state['name'] . '&nbsp;' . $country['name'] ?> </h1>                    </div>                                    </div> </div>

    <?php
    if ($temp == 1 && $temp1 == 1) {
        $listing = mysql_query($sql);
        if (mysql_num_rows($listing) > 0) {
            $x = array();
            while ($listing_rows = mysql_fetch_assoc($listing)) {
                $x[] = $listing_rows['id'];
            }
            $listing_id = implode(',', $x);
            $sql1 = "SELECT Listing_Summary.*  FROM Listing_Summary where id in (" . $listing_id . ")";
            $resultzip = $dbObj->query($sql1);
            $j = 0;
            $count = 1;
            ?>

            <?php
            if ($resultzip) {
                $alph = range("A", "Z");
                $j = 0;
                $LocationData = array();
                while ($list_det = mysql_fetch_assoc($resultzip)) {
                    $LocationData[] = array($list_det['longitude'], $list_det['latitude'], $list_det['address'], $list_det['title']);
                    if ($list_det['location_1'] != 0 || !empty($list_det['location_1'])) {
                        $name = mysql_query("select `name`,`abbreviation` from " . _DIRECTORYDB_NAME . ".Location_1 where id=" . $list_det['location_1']);
                        if ($name)
                            $country = mysql_fetch_assoc($name);
                    } if ($list_det['location_3'] != 0 || !empty($list_det['location_3'])) {
                        $name = mysql_query("select `name`,`abbreviation` from " . _DIRECTORYDB_NAME . ".Location_3 where id=" . $list_det['location_3']);
                        if ($name)
                            $state = mysql_fetch_assoc($name);
                    }
                    if ($list_det['location_4'] != 0 || !empty($list_det['location_4'])) {
                        $name = mysql_query("select `name`  from " . _DIRECTORYDB_NAME . ".Location_4 where id=" . $list_det['location_4']);
                        if ($name)
                            $city = mysql_fetch_assoc($name);
                    }
                    $data = mysql_query("SELECT category_root_id  FROM Listing_Category where listing_id = (" . $list_det['id'] . ")");
                    $row = mysql_fetch_assoc($data);
                    $root_id = $row['category_root_id'];
                    $data2 = mysql_query("SELECT title  FROM  ListingCategory where root_id = (" . $root_id . ")");
                    $row = mysql_fetch_assoc($data2);
                    $cat_title = $row['title'];
                    $data = mysql_query("SELECT category_root_id  FROM Listing_Category where listing_id = (" . $list_det['id'] . ")");
                    $row = mysql_fetch_assoc($data);
                    $root_id = $row['category_root_id'];
                    ?>
                    <div class="new_box" >
                        <span style="display: block; margin-right: -35px; padding: 1px 6px; width: 9px; height: 32px; background-image: url('http://<?php echo $_SERVER[HTTP_HOST] ?>/images/chart_1.png');"><?php echo $alph[$j]; ?></span>
                        <div class="new_head_box" style="font-size: 12px; float: left; margin-top: -28px; margin-left: 28px;" >
                            <div class="new_head2">
                                <a style="font-size:12px;text-decoration:underline;" href="http://<?= $_SERVER['HTTP_HOST'] ?>/listing/<?php echo $list_det['friendly_url'] ?>.html"><?= system_showTruncatedText($list_det['title'], 25); ?></a> 
                                <?= ($lisiting_subcate_det["title"]) ? '(' . $lisiting_subcate_det["title"] . ')' : ''; ?>					                        </div>
                                <?php echo $list_det['address']; ?> , <?php echo ($city['name']) ? $city['name'] . ',' : '' ?><?= $state['abbreviation']; ?> 
                            <br/>
                            <div class="new_site_link"><a href="tel:<?php echo $list_det['phone']; ?>"><?php echo $list_det['phone']; ?> </a>&nbsp;&nbsp;&nbsp; <a href="<?php echo $list_det['url'] ?>"><?php echo parse_url($list_det['url'], PHP_URL_HOST); ?></a>&nbsp;&nbsp;&nbsp;
                                <a onclick="javascript:window.open('http://maps.google.com/maps?q=<?php echo $list_det["latitude"] ?>,<?php echo $list_det["longitude"] ?>', 'popup', '')" href="javascript:void(0);">Get driving directions</a>

                            </div>
                        </div>
                        <div class="clear"></div>
                        <p style=" font-size: 11px;  line-height: 18px;margin-left:28px">
                            <?= system_showTruncatedText($list_det['description'], 100); ?>
                        </p>

                    </div>

                    <br/>
                    <?php
                    $j++;
                    $data = mysql_query("SELECT *  FROM Promotion where listing_id = (" . $list_det['id'] . ")");

                    if ($data) {
                        while ($row = mysql_fetch_assoc($data)) {
                            $dealname = $row['name'];
                            $promotionurl = "http://" . $_SERVER['HTTP_HOST'] . "/deal/" . $row["friendly_url"] . ".html";
//               print_r($list_det['friendly_url']);exit;
                            $deal_price = string_substr($row["dealvalue"], 0, (string_strpos($row["dealvalue"], ".")));
                            $deal_cents = string_substr($row["dealvalue"], (string_strpos($row["dealvalue"], ".")), 3);
                            $listing_link = "http://" . $_SERVER['HTTP_HOST'] . "/listing/" . $list_det['friendly_url'] . ".html";
                        }
                    }
                    ?>

                    <div style="clear:both"></div>	 
                    <?php
                }
            }
        }
    }
}
?>
