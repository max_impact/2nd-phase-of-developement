<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/views/view_event_detail_code.php
	# ----------------------------------------------------------------------------------------------------

    include(INCLUDES_DIR."/views/view_detail_tabs.php");
    
?>

    <div class="tab-container">

        <div id="content_overview" class="tab-content">

            <div class="top-info span12">
                
                <div clas="row-fluid">
                    <h3 class="span10"><?=$event_title;?></h3>
                    
                    <div class="span2 share-middle text-right">
                        <?=$event_icon_navbar?>
                    </div>
                </div>
                
            </div>

            <div class="row-fluid">
                
                <? if ($event_category_tree) { ?>
                <div class="span12 top-info">
                    <?=$event_category_tree?>
                </div>
                <? } ?>
                
            </div>

            <div class="row-fluid middle-info">

                <? if ($imageTag || $eventGallery) { ?>

                <div class="span7">

                    <? if (($imageTag && !$eventGallery && $onlyMain) || ($tPreview && $imageTag)) { ?>
                        <div class="image">
                            <?=$imageTag?>
                        </div>
                    <? } ?>

                    <? if ($eventGallery) { ?>
                        <div <?=$tPreview ? "class=\"ad-gallery gallery\"" : ""?>>
                            <?=$eventGallery?>
                        </div>
                    <? } ?>
                </div>

                <? } ?>

                <div class="span5">

                    <strong><?=system_showText(LANG_EVENT_WHEN);?>:</strong><br />
                    
                    <?=($event->getString("recurring") != "Y" ? $str_date : $str_recurring);?> <br />

                    <? if ($str_time) { ?>
                            <br /><strong><?=system_showText(LANG_EVENT_TIME)?>:</strong> <?=$str_time?> <br />
                    <? } ?>

                    <? if ($event_location || $location || $event_address || $event_address2 || $location_map) { ?>

                        <br /><strong><?=system_showText(LANG_LABEL_ADDRESS);?>:</strong>

                        <address>

                    <? } ?>
                            
                    <? if ($event_location) { ?>
                        <span><?=$event_location?></span><br />
                    <? } ?>

                    <? if ($event_address) { ?>
                        <span><?=nl2br($event_address)?></span><br />
                    <? } ?>

                    <? if ($event_address2) { ?>
                        <span><?=nl2br($event_address2)?></span><br />
                    <? } ?>

                    <? if ($location) { ?>
                        <span><?=$location?></span><br />
                    <? } ?>
                        
                    <? if ($location_map) { ?>
                        <? if ($user) { ?>
                            <a href="<?=$map_link?>" target="_blank"><?=system_showText(LANG_EVENT_DRIVINGDIRECTIONS)?></a><br />
                        <? } else { ?>
                            <a href="javascript:void(0);" style="cursor:default"><?=system_showText(LANG_EVENT_DRIVINGDIRECTIONS)?></a><br />
                        <? } ?>
                    <? } ?>

                    <? if ($event_location || $location || $event_address || $event_address2 || $location_map) { ?>
                        </address>
                    <? } ?>

                    <? if ($event_phone) { ?>
                        <strong><?=system_showText(LANG_LABEL_PHONE)?>:<br /></strong> <?=$event_phone?> <br />
                    <? } ?>

                    <? if ($event_url) { ?>
                       <br /><strong><?=system_showText(LANG_EVENT_WEBSITE)?>: <br /> </strong>
                        
                        <? if (!$user) {
                            echo "<address class=\"website\"><a href=\"javascript:void(0);\" style=\"cursor:default\">".$dispurl."</a></address>";
                        } else {
                            echo "<address class=\"website\"><a href=\"".$event_url."\" target=\"_blank\">".$event_url."</a></address>";
                        } ?> 

                    <? } ?>

                    <? if ($event_contactName) { ?>
                        <br /><strong><?=system_showText(LANG_LABEL_CONTACTNAME)?>:</strong><br /> <?=$event_contactName?> <br />
                    <? } ?>
                        
                    <? if ($event_email){ ?>
                        <br /><a rel="nofollow" href="<?=$contact_email;?>" class="<?=!$tPreview? "fancy_window_tofriend": "";?> btn btn-large btn-success" <?=(!$user ? "style=\"cursor:default;\"" : "");?>>
                            <?=system_showText(LANG_SEND_AN_EMAIL);?>
                        </a>
                    <? } ?>

                </div>

            </div>

            <div class="row-fluid">

                <? if ($event_description) { ?>
                    <div class="content-box">
                        <h4><?=system_showText(LANG_LABEL_DESCRIPTION);?></h4>
                        <p class="long"><?=$event_description?></p>
                    </div>
                <? } ?>

            </div>

        </div>

        <div id="content_video" class="tab-content hidden-phone" <?=$activeTab == "video"? "style=\"\"": "style=\"display: none;\"";?>>

            <div class="row-fluid">

                <div class="span12 top-info">
                    <h3><?=$event_title?></h3>
                </div>

                <div class="video">
                    <script language="javascript" type="text/javascript">
                    //<![CDATA[
                    document.write("<?=str_replace("\"","'", $event_video_snippet)?>");
                    //]]>
                    </script>
                </div>

            </div>

        </div>

    </div>