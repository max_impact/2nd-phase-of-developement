<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/forms/form_account_members.php
	# ----------------------------------------------------------------------------------------------------

    if(SELECTED_DOMAIN_ID==3){
        $ProfileContact = new AccountProfileContact($account_id);
      
        if (!$nonprofit_categories) if ($ProfileContact) $nonprofit_categories = $ProfileContact->getNonProfitCategories(false, false, $account_id, true, true);
//       print_r( $nonprofit_categories); exit;
        if ($nonprofit_categories) {
		for ($i=0; $i<count($nonprofit_categories); $i++) {
			$arr_category1[$i]["name"] = $nonprofit_categories[$i]->getString("title");
			$arr_category1[$i]["value"] = $nonprofit_categories[$i]->getNumber("id");
			$arr_return_categories1[] = $nonprofit_categories[$i]->getNumber("id");
		}
		if ($arr_return_categories1) $return_categories_nonprofit = implode(",", $arr_return_categories1);
		array_multisort($arr_category1);
		
                $feedDropDown1 = "<select name='feed1' id='feed1' multiple size='5' style=\"width:500px\">";
		if ($arr_category1) foreach ($arr_category1 as $each_category) {
			$feedDropDown1 .= "<option value='".$each_category["value"]."'>".$each_category["name"]."</option>";
			$feedAjaxCategory1[] = $each_category["value"];
		}
		$feedDropDown1 .= "</select>";
	} else {
		if ($return_categories_nonprofit) {
			$return_categories_array = explode(",", $return_categories_nonprofit);
			if ($return_categories_array) {
				foreach ($return_categories_array as $each_category) {
					$nonprofit_categories[] = new NonProfitListingCategory($each_category);
				}
			}
		}
		
                $feedDropDown1 = "<select name='feed1' id='feed1' multiple size='5' style=\"width:500px\">";
		if ($nonprofit_categories) {
			foreach ($nonprofit_categories as $category) {
				$name = $category->getString("title");
				$feedDropDown1 .= "<option value='".$category->getNumber("id")."'>$name</option>";
				$feedAjaxCategory1[] = $category->getNumber("id");
			}
		}
		$feedDropDown1 .= "</select>";
	}
        }
        
      
?>

<script language="javascript" type="text/javascript" src="<?=DEFAULT_URL?>/scripts/categorytree.js"></script>
<script language="javascript" type="text/javascript">
    	<?php     if(SELECTED_DOMAIN_ID==3){
            ?>	
            $(document).ready(function(){
                loadCategoryTreeNonProfit('all', 'nonprofitlisting_', 'NonProfitListingCategory', 0, 0, '<?=DEFAULT_URL."/".EDIR_CORE_FOLDER_NAME."/".LISTING_FEATURE_FOLDER?>',<?=SELECTED_DOMAIN_ID?>);
    
            })
        <?php } ?>
        
         
        function JS_addCategory1(id) {

		
               
		var  feed = document.account.feed1;
          
		
		var text = unescapeHTML($("#nonprofitliContent"+id).html());
		
			var flag=true;
			for (i=0;i<feed.length;i++)
				if (feed.options[i].value==id)
					flag=false;

			if(text && id && flag){
				feed.options[feed.length] = new Option(text, id);
				$('#nonprofitcategoryAdd'+id).after("<span class=\"categorySuccessMessage\"><?=system_showText(LANG_MSG_CATEGORY_SUCCESSFULLY_ADDED)?></span>").css('display', 'none');
				$('.categorySuccessMessage').fadeOut(5000);
			} else {
				if (!flag) $('#nonprofitcategoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\"><?=system_showText(LANG_MSG_CATEGORY_ALREADY_INSERTED)?></span> </li>");
				else ('#nonprofitcategoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\"><?=system_showText(LANG_MSG_SELECT_VALID_CATEGORY)?></span> </li>");
			}
		

        $('#removeCategoriesButton1').show(); 

	}
        
     function JS_removeCategory1(feed, order) {

    if (feed.selectedIndex >= 0) {
        $('#nonprofitcategoryAdd'+feed.options[feed.selectedIndex].value).after($('.categorySuccessMessage').empty());
        $('#nonprofitcategoryAdd'+feed.options[feed.selectedIndex].value ).fadeIn(500);
        feed.remove(feed.selectedIndex);
        if (order){
            orderCalculate();
        }
    }
    
	if(feed.length == 0){
    	$('#removeCategoriesButton1').hide();
    }

}
   
   
	function JS_submit() {
		
                <?php if(SELECTED_DOMAIN_ID==3){ ?>
                        feed1 = document.account.feed1;
                        return_categories_nonprofit= document.account.return_categories_nonprofit;
		if(return_categories_nonprofit.value.length > 0) return_categories_nonprofit.value="";

		for (i=0;i<feed1.length;i++) {
			if (!isNaN(feed1.options[i].value)) {
				if(return_categories_nonprofit.value.length > 0)
				return_categories_nonprofit.value = return_categories_nonprofit.value + "," + feed1.options[i].value;
				else
			return_categories_nonprofit.value = return_categories_nonprofit.value + feed1.options[i].value;
			}
		}
            
                <?php } ?>
		

		document.account.submit();
	}
        
           $(document).ready(function(){
                               $("a.fancy_window_invoice1").fancybox({
                    'overlayShow'			: true,
                    'overlayOpacity'		: 0.75,
                    'width'                 : 680,
                    'height'                : 480,
                        
                                                
                    'padding'               : 0,
                    'margin'                : 0,
                    'showCloseButton'       : true,

                    'titleShow'             : false
                });
           });
           
</script>

<!-- <script>

        
   
   
        
            </script>-->
<?php 
    $accountID = sess_getAccountIdFromSession();
//     print_r($nonprofit_sub_cate);exit;               
    $readonly = "";
    if (DEMO_LIVE_MODE && ($username == "demo@demodirectory.com")) {
        $readonly = "readonly"; 
    }

    $isForeignAcc = false;

    if ((string_strpos($username, "facebook::") !== false || string_strpos($username, "openid::") !== false || string_strpos($username, "google::") !== false)) {
        $isForeignAcc = true;
    }

    $dropdown_protocol = html_protocolDropdown($url, "url_protocol", false, $protocol_replace);

    ?>
        
    <script language="javascript" type="text/javascript" src="<?=DEFAULT_URL?>/scripts/checkusername.js"></script>
    <script language="javascript" type="text/javascript" src="<?=DEFAULT_URL?>/scripts/activationEmail.js"></script>
                          
    <? if ((string_strpos($username, "facebook::") === false && string_strpos($username, "openid::") === false && string_strpos($username, "google::") === false)) { ?>
    <div id="change-email">

        <div class="left textright">
            <h2><?=system_showText(LANG_LABEL_ACCOUNT_USERNAME);?></h2>
            <span><?=system_showText(LANG_LABEL_ACCOUNT_USERNAME_TIP);?></span>
        </div>

        <div class="right">

            <div class="cont_100">

                <label><?=system_showText(LANG_LABEL_USERNAME)?> <a href="javascript: void(0);">* <span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></a></label>

                <div class="checking">

                    <input type="text" name="username" value="<?=$username?>" maxlength="<?=USERNAME_MAX_LEN?>" onblur="checkUsername(this.value, '<?=DEFAULT_URL;?>', 'members', <?=($accountID ? $accountID : 0);?>); populateField(this.value,'email');"/>
                    <label id="checkUsername">&nbsp;</label>

                    <? if ($active == "y") { ?>
                        <span class="positive"> <img src="<?=DEFAULT_URL?>/images/ico-approve.png"/> <?=system_showText(LANG_LABEL_ACCOUNT_ACT);?></span>
                    <? } else { ?>
                        <span class="negative"> <img src="<?=DEFAULT_URL?>/images/ico-deny.png"/> <?=system_showText(LANG_LABEL_ACCOUNT_NOTACT);?> <a href="javascript: void(0);" onclick="sendEmailActivation(<?=$accountID?>);"><?=system_showText(LANG_LABEL_ACTIVATE_ACC);?></a></span>
                    <? } ?>
                    <img id="loadingEmail" src="<?=DEFAULT_URL?>/images/img_loading.gif" width="15px;" style="display: none;" />
                    <input type="hidden" name="active" value="<?=$active?>" />

                </div>

            </div>

        </div>

    </div>

    <? } else { ?>

    <input type="hidden" name="username" value="<?=$username?>" />

    <? } ?>

    <? if (!$isForeignAcc) { ?>

    <div id="change-password">

        <div class="left textright">

            <h2><?=system_showText(LANG_LABEL_ACCOUNT_CHANGEPASS);?></h2>
            <span><?=system_showText(LANG_LABEL_ACCOUNT_CHANGEPASS_TIP);?></span>

        </div>

        <div class="right">

            <div class="cont_100">

                <label><?=system_showText(LANG_LABEL_CURRENT_PASSWORD)?></label>

                <div class="checking">
                    <input type="password" name="current_password" class="input-form-account" <?=$readonly?> />
                </div>

            </div>

            <div class="cont_50">
                <label><?=system_showText(LANG_LABEL_NEW_PASSWORD);?></label>
                <input type="password" name="password" maxlength="<?=PASSWORD_MAX_LEN?>" <?=$readonly?> />
            </div>

            <div class="cont_50">
                <label><?=system_showText(LANG_LABEL_RETYPE_NEW_PASSWORD);?></label>
                <input type="password" name="retype_password" <?=$readonly?> />
            </div>

        </div>

    </div>

    <? } ?>

<?php //non profit
                if(SELECTED_DOMAIN_ID==3){ ?>
    <br/>
    <table style="margin:21px;float: left;clear: both;">
                <tr>
                    <th colspan="2" class="standard-tabletitle" style="text-align:left;">			
			<?=system_showText(LANG_CATEGORIES_SUBCATEGS1)?>
			
		</th>
	</tr>
	<tr>
		<td colspan="2" class="standard-tableContent">
                    <p class="warningBOXtext" style=" color: #494949; font-size: 14px;"><?=system_showText(LANG_MSG_ONLY_SELECT_SUBCAUSE)?> <?=system_showText(USER_MAX_CAUSE_ALLOWED);?><?=system_showText(LANG_MSG_ONLY_SELECT_SUBCAUSE1)?><br /></p>
		</td>
	</tr>
                
		<input type="hidden" name="return_categories_nonprofit" value="" />
                <tr>
			<td colspan="2" class="treeView">
				<ul id="nonprofitlisting_categorytree_id_0" class="categoryTreeview">&nbsp;</ul>

				<table width="100%" border="0" cellpadding="2" class="tableCategoriesADDED" cellspacing="0">
					<tr>
						<th colspan="2" class="tableCategoriesTITLE alignLeft"><i><span>Your Choosen Causes</span></i> </th>
					</tr>
					<tr id="msgback2" class="informationMessageShort">
						<td colspan="2" style="width: auto;"><p><img width="16" height="14" src="<?=DEFAULT_URL?>/images/icon_atention.gif" alt="<?=LANG_LABEL_ATTENTION?>" title="<?=LANG_LABEL_ATTENTION?>" /> <?=system_showText(LANG_MSG_CLICKADDTOSELECTCATEGORIES);?></p></td>
					</tr>
					<tr id="msgback" class="informationMessageShort" style="display:none">
						<td colspan="2"><i>  <span>Your Choosen Causes</span></i> <div><img width="16" height="14" src="<?=DEFAULT_URL?>/images/icon_atention.gif" alt="<?=LANG_LABEL_ATTENTION?>" title="<?=LANG_LABEL_ATTENTION?>" /></div><p><?=system_showText(LANG_MSG_CLICKADDTOADDCATEGORIES);?></p></td>
					</tr>
					<tr>
						<td colspan="2" class="tableCategoriesCONTENT"><?=$feedDropDown1?></td>
					</tr>
					<tr id="removeCategoriesButton1" <?=(($nonprofit_categories) ? "" :"style='display:none'") ?>>
						<td class="tableCategoriesBUTTONS" colspan="2">
							<center>
                                                            <button class="input-button-form"  style="background-color: #494949;" type="button" value="<?=system_showText(LANG_BUTTON_REMOVESELECTEDCATEGORY)?>" onclick="JS_removeCategory1(document.account.feed1, false);"><?=system_showText(LANG_BUTTON_REMOVESELECTEDCATEGORY)?></button>
							</center>
						</td>
					</tr>
				</table>
			</td>
		</tr>
                <tr>
                    <td style="text-align: center; padding: 18px;">
                        
                            <a class="iframe fancy_window_invoice1" href="<?php echo "http://".$_SERVER["HTTP_HOST"]."/content/suggest-cause.html" ;?>" >Don’t See Your Cause Suggest One Here </a>
                    </td>
                </tr>
		
                   </table>
                <? } ?>     
  
