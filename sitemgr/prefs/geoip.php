<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /sitemgr/prefs/geoip.php
	# ----------------------------------------------------------------------------------------------------

	# ----------------------------------------------------------------------------------------------------
	# LOAD CONFIG
	# ----------------------------------------------------------------------------------------------------
	include("../../conf/loadconfig.inc.php");
	
	# ----------------------------------------------------------------------------------------------------
	# SESSION
	# ----------------------------------------------------------------------------------------------------
	sess_validateSMSession();
	permission_hasSMPerm();

	# ----------------------------------------------------------------------------------------------------
	# VALIDATING FEATURES
	# ----------------------------------------------------------------------------------------------------
	extract($_POST);
	extract($_GET);	

	//increases frequently actions
	if ($_SERVER["REQUEST_METHOD"] != "POST") system_setFreqActions('prefs_geoip', 'prefsgeoip');
    
    # ----------------------------------------------------------------------------------------------------
	# SUBMIT
	# ----------------------------------------------------------------------------------------------------
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
        $fileConstPath = EDIRECTORY_ROOT."/custom/domain_".SELECTED_DOMAIN_ID."/conf/constants.inc.php";
        $constValues = array();
        $constValues["geoip_feature"] = ($geoip_feature ? "on" : "off");

        if (!system_writeConstantsFile($fileConstPath, SELECTED_DOMAIN_ID, $constValues)) {
            $error = true;
        }
            
        if ($error) {
            $message_geoip = "<p class=\"errorMessage\">".system_showText(LANG_SITEMGR_MSGERROR_SYSTEMERROR)."</p>";
        } else {
            
            if (CACHE_FULL_FEATURE == "on") {
                cachefull_forceExpiration();
            }
            
            header("Location: ".DEFAULT_URL."/".SITEMGR_ALIAS."/prefs/geoip.php?success=1");
            exit;
        }
    }
	
	# ----------------------------------------------------------------------------------------------------
	# HEADER
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/header.php");

	# ----------------------------------------------------------------------------------------------------
	# NAVBAR
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/navbar.php");
    
?>
    <div id="main-right">

		<div id="top-content">
			<div id="header-content">
				<h1><?=system_showText(LANG_SITEMGR_SETTINGS_SITEMGRSETTINGS)?> - <?=system_showText(LANG_SITEMGR_GEOIP)?></h1>
			</div>
		</div>

		<div id="content-content">
            
			<div class="default-margin">

				<?
                require(EDIRECTORY_ROOT."/".SITEMGR_ALIAS."/registration.php");
                require(EDIRECTORY_ROOT."/includes/code/checkregistration.php");
                require(EDIRECTORY_ROOT."/frontend/checkregbin.php");
                ?>

				<br />

				<form name="settings_geoip" action="<?=system_getFormAction($_SERVER["PHP_SELF"])?>" method="post">
                    
                    <p><?=system_showText(LANG_SITEMGR_SETTINGS_GEOIP_TIP1);?></p>
                    
                    <?  if ($message_geoip) {
                            echo $message_geoip;
                        } elseif ($success) {
                            echo "<p class=\"successMessage\">".system_showText(LANG_SITEMGR_SETTINGS_GEOIP_SUCCESS)."</p>";
                        }
                    ?>
                    
                    <br class="clear" />
                                           
                    <table cellpadding="2" cellspacing="0" border="0" class="standard-table">
                        <tr class="tr-form">
                            <th>
                                <input type="checkbox" name="geoip_feature" value="on" <?=(GEOIP_FEATURE == "on" ? "checked=\"checked\"" : "");?> class="inputCheck">
                            </th>
                            <td><?=system_showText(LANG_SITEMGR_SETTINGS_GEOIP_ENABLE)?></td>
                        </tr>
                    </table>
                    
					<table style="margin: 0 auto 0 auto;">
						<tr>
							<td>
								<button type="submit" name="settings_geoip" value="Submit" class="input-button-form"><?=system_showText(LANG_SITEMGR_SUBMIT)?></button>
							</td>
						</tr>
					</table>
                    
				</form>

			</div>
		</div>

		<div id="bottom-content">
			&nbsp;
		</div>

	</div>

<?
	# ----------------------------------------------------------------------------------------------------
	# FOOTER
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/footer.php");
?>