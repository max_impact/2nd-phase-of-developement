INSERT INTO `BannerLevel` (`value`, `name`, `defaultlevel`, `price`, `width`, `height`, `impression_block`, `impression_price`, `active`, `content`, `displayName`, `theme`) VALUES
(1, 'top', 'y', 50.00, 728, 90, 1000, 50.00, 'y', 'Banner size: 728px x 90px', 'top', 'default' ),
(2, 'bottom', 'n', 20.00, 468, 60, 1000, 20.00, 'y', 'Banner size: 468px x 60px', 'bottom', 'default' ),
(3, 'top right', 'n', 40.00, 120, 90, 1000, 40.00, 'y', 'Banner size: 120px x 90px', 'top right', 'default' ),
(50, 'sponsored links', 'n', 10.00, 180, 66, 1000, 10.00, 'y', 'Banner size: 180px x 66px', 'sponsored links', 'default'),
(2, 'bottom', 'y', 20.00, 468, 60, 1000, 20.00, 'y', 'Banner size: 468px x 60px', 'bottom', 'realestate'),
(3, 'featured', 'n', 40.00, 180, 150, 1000, 40.00, 'y', 'Banner size: 180px x 150px', 'featured', 'realestate'),
(50, 'sponsored links', 'n', 10.00, 180, 100, 1000, 10.00, 'y', 'Banner size: 180px x 100px', 'sponsored links', 'realestate'),
(1, 'top', 'y', 50.00, 728, 90, 1000, 50.00, 'y', 'Banner size: 728px x 90px', 'top', 'diningguide' ),
(2, 'bottom', 'n', 20.00, 468, 60, 1000, 20.00, 'y', 'Banner size: 468px x 60px', 'bottom', 'diningguide' ),
(3, 'top right', 'n', 40.00, 120, 90, 1000, 40.00, 'y', 'Banner size: 120px x 90px', 'top right', 'diningguide' ),
(50, 'sponsored links', 'n', 10.00, 180, 66, 1000, 10.00, 'y', 'Banner size: 180px x 66px', 'sponsored links', 'diningguide');